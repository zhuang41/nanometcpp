#pragma once

#include <opencv2/core/core.hpp>

class Particle {
public:
	/**
	 * @brief	default constructor
	 */
	Particle();

	/**
	* @brief	default deconstructor
	*/
	~Particle();

	/* 
	 * @brief		initialize parameters 
	 * @param[in]	filename: the full filename of the image file
	 * @param[in]	ksize: the size of the median filter
	 * @param[in]	minLength: the minimum length of a valid line segment linking a pair of in and out points
	 * @param[in]	maxLength: the maximum length of a valid line segment linking a pair of in and out points
	 * @param[in]	minUpIdx: the minimum upward length of a verticle chord
	 * @param[in]	minDownIdx: the minimum downward length of a verticle chord
	 * @param[in]	stepsize: the step size of the sampling in rows
	 * @param[in]	tol: the threshold (standard deviation) used for rejecting erroneously fitted circles due to noise
	 * @param[in]	maxDist: the threshold used for circle merging  
	 */
	void setupParameters(const std::string filename, int ksize, int minLength, 
		int maxLength, int minUpIdx, int minDownIdx, int stepSize, double tol, int maxDist);

	/* 
	 * @brief	run the whole pipeline
	 * @output	the private member m_circles
	 */
	void run();

	/*
	 * @brief		visualize the results
	 */
	void visualizeResults(double resizeFactor);

	/*
	 * @brief	save the final circles
	 */
	void saveResults();

	/*
	* @brief	get the final circles
	* @return	the final circles
	*/
	const std::vector<cv::Vec3d>& getCircles() const { return m_circles; };

	/*
	* @brief	get the binary image
	* @return	the binary image
	*/
	const cv::Mat& getBinaryImage() const { return m_binaryImage; };

	/*
	* @brief	get the gray-scale image
	* @return	the gray-scale image
	*/
	const cv::Mat& getGrayScaleImage() const { return m_grayScaleImage; };

private:
	/*
	* @brief	1. load the original image I by the filename
	* 			2. covert I into a grayscale image I_g
	* 			3. obtain the binary image I_b using otsu
	* 			4. remove noise from I_b using a median filter
	* @input	the private member m_filename
	* @output	the private member m_binaryImage
	*/
	void loadImage();

	/*
	* @brief	find the masks of the in-points and out-points using a 1-D filter [-1, 1], respectively
	* @input	the private member m_binaryImage
	* @output	the private member m_inPtMask
	* @output	the private member m_outPtMask
	*/
	void findInOutPointMasks();

	/*
	* @brief			find the in and out points of a scan line (row) of the binary image
	* @param[in]		row: the row index of the scan line
	* @param[in, out]  inPtIdx: the column indices of the in points
	* @param[in, out]	outPtIdx: the column indices of the out points
	* @note			The size of inPtIdx and that of outPtIdx are the same.
	* @return			true if the size is not 0; false otherwise
	*/
	bool findInOutPointsInLine(int row, std::vector<cv::Point>& inPtIdx, std::vector<cv::Point>& outPtIdx);

	/*
	* @brief			fit a circle using [Taubin91toami]
	* @ref				G. Taubin, "Estimation Of Planar Curves, Surfaces And Nonplanar Space Curves Defined By Implicit Equations, With
	*					Applications To Edge And Range Image Segmentation", IEEE Trans. PAMI, Vol. 13, pages 1115-1138, (1991)
	* @param[in]		points: a vector of points uesed for fitting the circle
	* @param[in, out]	circle: the fitted circle <cx, cy, r> with <cx, cy> = <circle.val[0], circle.val[1]> and r = circle.val[2]
	* @return			true if a valid fitted circle exists; false otherwise
	*/
	bool fitCircle(const std::vector<cv::Point>& points, cv::Vec3d& circle);

	/*
	* @brief			detect a circle using a valid line segement linking one pair of in and out points
	* @param[in]		col1: the column index of the in point (start point)
	* @param[in]		col2: the column index of the out point (end point)
	* @param[in]		row: the row index
	* @param[in, out]	circle: the fitted circle <cx, cy, r> with <cx, cy> = <circle.val[0], circle.val[1]> and r = circle.val[2]
	* @return			true if a valid fitted circle exists; false otherwise
	*/
	bool detectCircle(int col1, int col2, int row, cv::Vec3d& circle);

	/*
	* @brief		merge circles
	* @param[in]	circle: newly detected circle
	* @output		the private member m_allCircles
	*/
	void mergeCircles(const cv::Vec3d& circle);

	/*
	* @brief		write an matrix into a txt file
	* @param[in]	m: input matrix
	* @param[in]	filename: the full filename of the file to be written
	*/
	void writeMatToFile(const cv::Mat& m, const std::string filename);


	cv::Mat m_grayScaleImage;	// gray-scale image
	cv::Mat m_binaryImage;		// binary image

	cv::Mat m_inPtMask;		// mask of in points
	cv::Mat m_outPtMask;	// mask of out points

	std::string m_filename;	// full filename of the original image

	int m_ksize;		// size of the median filter			

	int m_minLength;	// the minimum length of a valid line segment linking a pair of in and out points
	int m_maxLength;	// the maximum length of a valid line segment linking a pair of in and out points

	int m_minUpIdx;		// the minimum upward length of a verticle chord
	int m_minDownIdx;	// the minimum downward length of a verticle chord

	int m_stepSize;		// the step size of the sampling in rows
	double m_tol;		// the threshold (standard deviation) used for rejecting erroneously fitted circles due to noise

	int m_maxDist;		// the threshold used for circle merging  

	std::vector<std::vector<cv::Vec3d> > m_allCircles;	// all circle candidates
	std::vector<cv::Vec3d> m_circles;	// final circles
};

