// nano-particle.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Particle.h"

#include <stdlib.h>
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	// define parameters with default values
	string filename = "C:/Users/Yao/Documents/Data/nano/particle/1.jpg";
	int ksize = 7;
	int minLength = 50;
	int maxLength = 100;
	int minUpIdx = 2;
	int minDownIdx = 2;
	int stepSize = 10;
	double tol = 5.0;
	int maxDist = 20;

	double resizeFactor = -1.0;

	// parse argument
	if (argc == 3) {
		filename = string(argv[1]);
		resizeFactor = atof(argv[2]);
	} 

	// define class instance
	Particle particle;

	// set up parameters
	particle.setupParameters(filename, ksize, minLength, maxLength, 
		minUpIdx, minDownIdx, stepSize, tol, maxDist);

	// run
	particle.run();

	// visualize results
	particle.visualizeResults(resizeFactor);

	// save results
	particle.saveResults();

    return 0;
}

