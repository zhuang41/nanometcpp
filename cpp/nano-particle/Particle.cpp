#include "stdafx.h"

#include "Particle.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>


using namespace std;


Particle::Particle() {
}


Particle::~Particle() {
}

void Particle::setupParameters(const std::string filename, int ksize, int minLength, 
	int maxLength, int minUpIdx, int minDownIdx, int stepSize, double tol, int maxDist) {
	// load parameters
	m_filename = filename;
	m_ksize = ksize;
	m_minLength = minLength;
	m_maxLength = maxLength;
	m_minUpIdx = minUpIdx;
	m_minDownIdx = minDownIdx;
	m_stepSize = stepSize;
	m_tol = tol;
	m_maxDist = maxDist;
}

void Particle::loadImage() {
	// load the original image
	cv::Mat image = cv::imread(m_filename, -1);

	// convert it to gray-scale if it has more than 3 channels
	if (image.channels() == 3) {
		cv::cvtColor(image, m_grayScaleImage, CV_RGB2GRAY);
	}
	else {
		m_grayScaleImage = image.clone();	// might not be very efficient
	}

	// get the binary image using otsu (background is black and foreground is white)
	double threshold = cv::threshold(m_grayScaleImage, m_binaryImage,
		128, 255, cv::ThresholdTypes::THRESH_OTSU + cv::ThresholdTypes::THRESH_BINARY_INV);

	// blur the binary image
	cv::medianBlur(m_binaryImage, m_binaryImage, m_ksize);
}

void Particle::findInOutPointMasks() {
	// calculate the difference matrix
	cv::Mat kernel = (cv::Mat_<float>(1, 2) << -1.f, 1.f);	// kernel
	cv::Mat diffMat;	// difference matrix
	cv::filter2D(m_binaryImage, diffMat, CV_64FC1, kernel, cv::Point(0, 0), 0.0);

	// find the mask of in-points (0-->255)
	cv::compare(diffMat, 255.0, m_inPtMask, cv::CmpTypes::CMP_EQ);
	m_inPtMask = m_inPtMask(cv::Rect(0, 0, m_inPtMask.cols - 1, m_inPtMask.rows));

	// find the mask of out-points (255-->0)
	cv::compare(diffMat, -255.0, m_outPtMask, cv::CmpTypes::CMP_EQ);
	m_outPtMask = m_outPtMask(cv::Rect(0, 0, m_outPtMask.cols - 1, m_outPtMask.rows));
}

/// TODO: improve the performance
bool Particle::findInOutPointsInLine(int row, vector<cv::Point>& inPtIdx, vector<cv::Point>& outPtIdx) {

	// get the r-th row (line) of the in-point masks
	cv::Mat inPtMaskRow = m_inPtMask.row(row);
	cv::findNonZero(inPtMaskRow, inPtIdx);

	// get the r-th row (line) of the out-point masks
	cv::Mat outPtMaskRow = m_outPtMask.row(row);
	cv::findNonZero(outPtMaskRow, outPtIdx);

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;

	// remove the left boundary regions
	if (inPtIdx[0].x > outPtIdx[0].x) {
		outPtIdx.erase(outPtIdx.begin());
	}

	// remove the right boundary regions
	if (inPtIdx.back().x > outPtIdx.back().x) {
		inPtIdx.pop_back();
	}

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;

	// remove point pair whose distance is not within the range
	assert(inPtIdx.size() == outPtIdx.size());	
	for (size_t k = 0; k < inPtIdx.size(); ) {
		int dist = outPtIdx[k].x - inPtIdx[k].x; 
		if (dist <= m_minLength || dist >= m_maxLength) { 
			// erase the invalid pair
			inPtIdx.erase(inPtIdx.begin() + k);
			outPtIdx.erase(outPtIdx.begin() + k);
		} else {
			k++;
		}
	}

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;
	else
		return true;
}

bool Particle::fitCircle(const std::vector<cv::Point>& points, cv::Vec3d & circle) {
	// get the number of all points
	size_t N = points.size();

	// create a 2-channel matrix consisting of all points in double
	cv::Mat tempPointMat(points);
	cv::Mat pointMat;
	tempPointMat.convertTo(pointMat, CV_64FC2);

	// calculate the centroid of all points
	cv::Scalar centroid = cv::mean(pointMat);

	// calculate the normalized moments
	double Mxx = 0.0;
	double Myy = 0.0;
	double Mzz = 0.0;

	double Mxy = 0.0;
	double Myz = 0.0;
	double Mzx = 0.0;

	for (int k = 0; k < N; k++) {
		cv::Scalar diff = pointMat.at<cv::Scalar>(k, 0) - centroid;
		double xi = diff.val[0];
		double yi = diff.val[1];
		double zi = xi * xi + yi * yi;
		
		Mxx += xi * xi;
		Myy += yi * yi;
		Mzz += zi * zi;

		Mxy += xi * yi;
		Myz += yi * zi;
		Mzx += zi * xi;
 	}
	Mxx = Mxx / static_cast<double>(N);
	Myy = Myy / static_cast<double>(N);
	Mzz = Mzz / static_cast<double>(N);

	Mxy = Mxy / static_cast<double>(N);
	Myz = Myz / static_cast<double>(N);
	Mzx = Mzx / static_cast<double>(N);

	// calculate the coefficients of the characteristic polynomial
	double Mz = Mxx + Myy;
	double covXY = Mxx * Myy - Mxy * Mxy;
	double A3 = 4.0 * Mz;
	double A2 = -3.0 * Mz * Mz - Mzz;
	double A1 = Mzz * Mz + 4.0 * covXY * Mz - Mzx *Mzx - Myz * Myz - Mz * Mz * Mz;
	double A0 = Mzx * Mzx * Myy + Myz * Myz * Mxx - Mzz * covXY - 2.0 * Mzx * Myz * Mxy + Mz * Mz * covXY;
	double A22 = 2.0 * A2;
	double A33 = 3.0 * A3;

	// apply Newton's method
	double xNew = 0.0;
	double yNew = 1e20;
	double epsilon = 1e-12;
	double tol = 1e-9;
	int maxIters = 20;

	for (int k = 0; k < maxIters; k++) {
		double yOld = yNew;
		yNew = A0 + xNew * (A1 + xNew * (A2 + xNew * A3));
		if (fabs(yNew) > fabs(yOld)) {
			xNew = 0.0;
			break;
		}

		double Dy = A1 + xNew * (A22 + xNew * A33);
		if (fabs(Dy) < tol) {
			return false;
		}

		double xOld = xNew;
		xNew = xOld - yNew / Dy;
		if (fabs((xOld - xNew) / xNew) < epsilon) {
			break;
		}
		if (k == maxIters - 1) {
			xNew = 0.0;
		}
		if (xNew < 0.0) {
			xNew = 0.0;
		}
	}

	// calculate the parameters of the circle
	double det = xNew * xNew - xNew * Mz + covXY;
	if (fabs(det) < tol) {
		return false;
	}
	double dispX = (Mzx * (Myy - xNew) - Myz * Mxy) / (2.0 * det);
	double dispY = (Myz * (Mxx - xNew) - Mzx * Mxy) / (2.0 * det);

	double centerX = dispX + centroid.val[0];
	double centerY = dispY + centroid.val[1];
	double radius = std::sqrt(dispX * dispX + dispY * dispY + Mz);

	circle.val[0] = centerX;
	circle.val[1] = centerY;
	circle.val[2] = radius;

	return true;
}

bool Particle::detectCircle(int col1, int col2, int row, cv::Vec3d& circle) {
	// calculate values
	double segLength = static_cast<double>(col2 - col1);	// length of the candidate line segment
	double skippedLength = segLength / 10.0;
	vector<cv::Scalar> circleCandidates;

	// loop
	int startColIdx = cvCeil(static_cast<double>(col1) + skippedLength);
	int endColIdx = cvFloor(static_cast<double>(col2) - skippedLength) + 1;
	vector<cv::Vec3d> circles;
	for (int col = startColIdx; col < endColIdx; col++) {
		cv::Mat tempUpLine = m_binaryImage(cv::Rect(col, 0, 1, row));
		cv::Mat upLine;
		cv::flip(tempUpLine, upLine, 0);	// flip the upward line
		cv::Mat downLine = m_binaryImage(cv::Rect(col, row + 1, 1, m_binaryImage.rows - row - 1));

		cv::Point upMinLoc;
		cv::minMaxLoc(upLine, 0, 0, &upMinLoc, 0);
		cv::Point downMinLoc;
		cv::minMaxLoc(downLine, 0, 0, &downMinLoc, 0);
		int upIdx = upMinLoc.y + 1;
		int downIdx = downMinLoc.y + 1;

		// create 3 conditions
		bool condition1 = upIdx > m_minUpIdx && downIdx > m_minDownIdx;
		bool condition2 = static_cast<double>(upIdx + downIdx) > (0.8 * segLength) &&
			static_cast<double>(upIdx + downIdx) < (2.0 * segLength);
		bool condition3 = (row - upIdx) > m_stepSize - 1 && (row + downIdx) < (m_binaryImage.rows - m_stepSize - 1);

		// if all conditions are satisfied
		if (condition1 && condition2 && condition3) {
			// add 4 points for a candidate circle
			vector<cv::Point> points;
			points.push_back(cv::Point(col1 + 1, row));
			points.push_back(cv::Point(col2, row));
			points.push_back(cv::Point(col, row - upIdx));
			points.push_back(cv::Point(col, row + downIdx));

			// fit circles using 3 points
			// TODO: improve the code in this part
			vector<cv::Vec3d> candidateCircles;
			for (size_t k = 0; k < points.size(); k++) {
				vector<cv::Point> tempPoints = points;
				tempPoints.erase(tempPoints.begin() + k);
				cv::Vec3d circle;
				bool isCircleValid = fitCircle(tempPoints, circle);
				if (isCircleValid) {
					candidateCircles.push_back(circle);
				} 
			}

			// calculate the standard deviation of the circle parameters
			if (candidateCircles.size() != 0) {
				cv::Mat tempMat(candidateCircles);
				cv::Scalar mean;
				cv::Scalar stdDev;
				cv::meanStdDev(tempMat, mean, stdDev);

				if (stdDev.val[0] < m_tol && stdDev.val[1] < m_tol && stdDev.val[2] < m_tol) {
					circles.push_back(cv::Vec3d(mean.val[0], mean.val[1], mean.val[2]));
				}
			}
		}
	}

	// calcualte the final parameters of the circle
	if (circles.size() == 0) {
		return false;
	} else {
		cv::Mat tempMat(circles);
		cv::Scalar mean;
		cv::Scalar stdDev;
		cv::meanStdDev(tempMat, mean, stdDev);

		circle.val[0] = mean.val[0];
		circle.val[1] = mean.val[1];
		circle.val[2] = mean.val[2];

		return true;
	}
}

void Particle::mergeCircles(const cv::Vec3d& circle) {
	// deal with the first valid circle
	if (m_allCircles.size() == 0) {
		vector<cv::Vec3d> tempVec;
		tempVec.push_back(circle);
		m_allCircles.push_back(tempVec);
		return;
	}

	// deal with other valid circles
	bool isCircleNew = true;
	for (int k = 0; k < m_allCircles.size(); k++) {
		cv::Mat tempMat(m_allCircles[k]);
		cv::Scalar mean;
		cv::Scalar stdDev;
		cv::meanStdDev(tempMat, mean, stdDev);
		double dx = circle.val[0] - mean.val[0];
		double dy = circle.val[1] - mean.val[1];
		double dist2 = dx * dx + dy * dy;
		if (dist2 < m_maxDist * m_maxDist) {
			m_allCircles[k].push_back(circle);
			isCircleNew = false;
			break;
		}
	}
	if (isCircleNew) {
		vector<cv::Vec3d> tempVec;
		tempVec.push_back(circle);
		m_allCircles.push_back(tempVec);
	}
}

void Particle::run() {
	// load image
	loadImage();

	// find the mask of in and out points
	findInOutPointMasks();

	// loop to detect all circles
	for (int row = m_stepSize - 1; row < m_binaryImage.rows - m_stepSize; row += m_stepSize) {
		std::vector<cv::Point> inPtIdx;
		std::vector<cv::Point> outPtIdx;
		bool isOK = findInOutPointsInLine(row, inPtIdx, outPtIdx);
		if (!isOK) {
			continue;
		}
		for (size_t k = 0; k < inPtIdx.size(); k++) {
			cv::Vec3d circle;
			bool isCircleFound = detectCircle(inPtIdx[k].x, outPtIdx[k].x, row, circle);	
			if (isCircleFound) {
				mergeCircles(circle);
			}
		}
	}

	// get the final circles by taking the mean
	for (size_t k = 0; k < m_allCircles.size(); k++) {
		cv::Mat tempMat(m_allCircles[k]);
		cv::Scalar mean;
		cv::Scalar stdDev;
		cv::meanStdDev(tempMat, mean, stdDev);
		m_circles.push_back(cv::Vec3d(mean.val[0], mean.val[1], mean.val[2]));
	}
}

void Particle::visualizeResults(double resizeFactor) {
	// draw final circles
	cv::Mat resultImage;
	cv::cvtColor(m_binaryImage, resultImage, CV_GRAY2RGB);
	for (size_t k = 0; k < m_circles.size(); k++) {
		cv::Vec3d circle = m_circles[k];
		int cx = static_cast<int>(circle.val[0]);
		int cy = static_cast<int>(circle.val[1]);
		int r = static_cast<int>(circle.val[2]);
		cv::circle(resultImage, cv::Point(cx, cy), r, CV_RGB(255, 0, 0), 4);
	}

	if (resizeFactor > 0.0 && resizeFactor < 1.0) {
		cv::resize(resultImage, resultImage, cv::Size(0, 0), resizeFactor, resizeFactor);
	}

	// save the resulting image
	cv::imwrite("ResultCPP.jpg", resultImage);
}

void Particle::saveResults() {
	// define the name of the file
	const string filename = "ResultsCPP.txt";

	// check whether it exists
	ifstream fin(filename.c_str());
	if (fin) {
		remove(filename.c_str());
	}

	// write the results
	ofstream fout(filename.c_str());
	if (!fout) {
		cout << "Cannot open file " << endl;
		return;
	}
	for (size_t k = 0; k < m_circles.size(); k++) {
		double confidence = static_cast<double>(m_allCircles[k].size()) / 4.0;
		if (confidence > 1.0) {
			confidence = 1.0;
		}
		fout << fixed;
		fout << setprecision(6);
		fout << m_circles[k].val[0] << "\t" << m_circles[k].val[1] << "\t" << m_circles[k].val[2] << "\t" << confidence << endl;
	}
	fout.close();
}

void Particle::writeMatToFile(const cv::Mat& m, const string filename) {
	ofstream fout(filename.c_str());

	if (!fout) {
		cout << "Cannot open file " << endl;  
		return;
	}

	for (int r = 0; r < m.rows; r++) {
		for (int c = 0; c < m.cols; c++) {
			if (m.type() == CV_8U) {
				fout << static_cast<double>(m.at<uchar>(r, c)) << "\t";
			} 
			if (m.type() == CV_64F) {
				fout << m.at<double>(r, c) << "\t";
			}
		}
		fout << endl;
	}
	fout.close();
}

