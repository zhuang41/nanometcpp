﻿#include "stdafx.h"

#include "../nano-particle/Particle.h"

#include "CppUnitTest.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace TestParticle {		
	TEST_CLASS(UnitTests) {
	public:
		
		//< test whether the difference matrix is correct
		TEST_METHOD(TestDiffMat) {
			// create a test image
			cv::Mat image = (cv::Mat_<uchar>(4, 4) << 0, 255, 255, 0,
													  0, 255, 255, 0, 
													  0, 255, 255, 0, 
				                                      0, 255, 255, 0);
			// create the kernel
			cv::Mat kernel = (cv::Mat_<float>(1, 2) << -1.f, 1.f);

			// get the difference
			cv::Mat diffMat;
			cv::filter2D(image, diffMat, CV_64FC1, kernel, cv::Point(0, 0), 0.0, cv::BorderTypes::BORDER_REPLICATE);

			// check the type of difference matrix
			Assert::AreEqual(diffMat.type(), CV_64FC1);

			// check the results
			Assert::AreEqual(-255.0, diffMat.at<double>(0, 2));
		}

		//< test whether the core functionality of finding in-points is correct
		TEST_METHOD(TestInPt) {
			// create a matrix
			cv::Mat diffMat = (cv::Mat_<double>(4, 4) << 255.0, 0.0, -255.0, 0.0,
													     255.0, 0.0, -255.0, 0.0,
													     255.0, 0.0, 255.0, 0.0,
													     255.0, 0.0, -255.0, 0.0);

			// find the in-points
			cv::Mat inPtMask;
			cv::compare(diffMat, 255.0, inPtMask, cv::CmpTypes::CMP_EQ);

			// check the type of matrix
			Assert::AreEqual(CV_8U, inPtMask.type());

			// check the results
			Assert::AreEqual(inPtMask.at<uchar>(0, 0), uchar(255));
			Assert::AreEqual(inPtMask.at<uchar>(1, 1), uchar(0));
			Assert::AreEqual(inPtMask.at<uchar>(2, 2), uchar(255));
			Assert::AreEqual(inPtMask.at<uchar>(3, 3), uchar(0));

			// check the function findnonzeros
			cv::Mat inPtMaskRow = inPtMask.row(2);
			std::vector<cv::Point> inPtIdx;
			cv::findNonZero(inPtMaskRow, inPtIdx);
			Assert::AreEqual(size_t(2), inPtIdx.size());
		}

		//< test whether the core functionality of finding out-points is correct
		TEST_METHOD(TestOutPt) {
			// create a matrix
			cv::Mat diffMat = (cv::Mat_<double>(4, 4) << 255.0, 0.0, -255.0, 0.0,
														 255.0, 0.0, -255.0, 0.0,
														 255.0, 0.0, -255.0, 0.0,
														 255.0, 0.0, -255.0, 0.0);

			// find the out-points
			cv::Mat outPtMask;
			cv::compare(diffMat, -255.0, outPtMask, cv::CmpTypes::CMP_EQ);

			// check the results
			Assert::AreEqual(outPtMask.at<uchar>(0, 2), uchar(255));
			Assert::AreEqual(outPtMask.at<uchar>(0, 0), uchar(0));
			Assert::AreEqual(outPtMask.at<uchar>(1, 1), uchar(0));
			Assert::AreEqual(outPtMask.at<uchar>(3, 3), uchar(0));

		}

		//< test the conversion of vector of points to the mat format
		TEST_METHOD(TestPointVectorMatConversion) {
			// create a vector of cv::Point
			vector<cv::Point> testVector;
			testVector.push_back(cv::Point(20, 30));
			testVector.push_back(cv::Point(40, 50));
			testVector.push_back(cv::Point(60, 70));
			testVector.push_back(cv::Point(80, 90));
			testVector.push_back(cv::Point(100, 110));

			// create the corresponding matrix
			cv::Mat testMat(testVector);

			// test the number of channels 
			Assert::AreEqual(2, testMat.channels());

			// test data type
			Assert::AreEqual(CV_32SC2, testMat.type());

			// test demension of the matrix
			Assert::AreEqual(5, testMat.rows);
			Assert::AreEqual(1, testMat.cols);

			// test conversion from int to double
			cv::Mat testMatDouble;
			testMat.convertTo(testMatDouble, CV_64FC2);
			Assert::AreEqual(20.0, testMatDouble.at<cv::Scalar>(0, 0).val[0]);
			Assert::AreEqual(50.0, testMatDouble.at<cv::Scalar>(1, 0).val[1]);

			// test the mean
			cv::Scalar meanVal = cv::mean(testMatDouble);
			Assert::AreEqual(60.0, meanVal.val[0]);
			Assert::AreEqual(70.0, meanVal.val[1]);
		}

		//< test fitting circle
		TEST_METHOD(TestMatOperations) {
			// create the test matrix
			cv::Mat testMat = 
				(cv::Mat_<double>(6, 6) <<  1.0,  3.0,  5.0,  7.0,  9.0, 11.0,
										   13.0, 15.0, 17.0, 19.0, 21.0, 23.0,
										   25.0, 27.0, 29.0, 31.0, 33.0, 35.0,
										   37.0, 39.0, 41.0, 43.0, 45.0, 47.0,
										   49.0, 51.0, 53.0, 55.0, 57.0, 59.0,
										   61.0, 63.0, 65.0, 67.0, 69.0, 71.0);
			int col = 3;
			int row = 2;
			cv::Mat upLine = testMat(cv::Rect(col, 0, 1, row));
			cv::Mat downLine = testMat(cv::Rect(col, row + 1, 1, testMat.rows - row - 1));

			// test the dimensions of lines
			Assert::AreEqual(row, upLine.rows);
			Assert::AreEqual(1, upLine.cols);
			Assert::AreEqual(testMat.rows - row - 1, downLine.rows);
			Assert::AreEqual(1, downLine.cols);

			// test values
			Assert::AreEqual(7.0, upLine.at<double>(0, 0));
			Assert::AreEqual(19.0, upLine.at<double>(1, 0));

			Assert::AreEqual(43.0, downLine.at<double>(0, 0));
			Assert::AreEqual(55.0, downLine.at<double>(1, 0));
			Assert::AreEqual(67.0, downLine.at<double>(2, 0));

			// test flip
			cv::Mat testColVec;
			cv::flip(testMat.col(0), testColVec, 0);
			Assert::AreEqual(61.0, testColVec.at<double>(0, 0));
			Assert::AreEqual(49.0, testColVec.at<double>(1, 0));
			Assert::AreEqual(37.0, testColVec.at<double>(2, 0));
			Assert::AreEqual(25.0, testColVec.at<double>(3, 0));
			Assert::AreEqual(13.0, testColVec.at<double>(4, 0));
			Assert::AreEqual(1.0, testColVec.at<double>(5, 0));
		}

		TEST_METHOD(TestMinMaxLoc) {
			cv::Mat testMat =
				(cv::Mat_<uchar>(6, 1) << 255, 255, 255, 0, 0, 0);
			cv::Point minLoc;
			cv::minMaxLoc(testMat, 0, 0, &minLoc, 0);
			Assert::AreEqual(3, minLoc.y);
		}

		TEST_METHOD(Testsize_t) {
			size_t num = 0;
			num--;
			num++;
			Assert::AreEqual(size_t(0), num);
		}

		TEST_METHOD(TestVectorOperation) {
			vector<cv::Vec3d> testVector;
			testVector.push_back(cv::Vec3d(20.0, 20.0, 40.0));
			testVector.push_back(cv::Vec3d(50.0, 60.0, 70.0));
			testVector.push_back(cv::Vec3d(80.0, 90.0, 100.0));
			testVector.push_back(cv::Vec3d(110.0, 120.0, 130.0));
			testVector.push_back(cv::Vec3d(140.0, 150.0, 160.0));

			// check the size
			Assert::AreEqual(size_t(5), testVector.size());

			// erase one element
			testVector.erase(testVector.begin() + 2);
			Assert::AreEqual(size_t(4), testVector.size());
		}

		typedef cv::Vec<double, 7> Vec7d;
		TEST_METHOD(TestMultipleChannel) {
			vector<Vec7d> tempVec;
			tempVec.push_back(Vec7d::Vec(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));
			tempVec.push_back(Vec7d::Vec(8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0));
			tempVec.push_back(Vec7d::Vec(15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0));
			tempVec.push_back(Vec7d::Vec(22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0));
			cv::Mat testMat(tempVec);
			
			Assert::AreEqual(7, testMat.channels());

			vector<cv::Mat> channels;
			cv::split(testMat, channels);	

			Assert::AreEqual(4, channels[3].rows);
			Assert::AreEqual(1, channels[3].cols);
		}

	};
}