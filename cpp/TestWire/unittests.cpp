#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestWire {		
	TEST_CLASS(UnitTests) {
	public:
		
		TEST_METHOD(TestLoop) {
			int rows = 1651;
			int cols = 2112;
			double bounds[4] = { 772.589449, 767.937500, -1633.099072, -2416.585138 };
			int x0 = 1052;
			int x1 = 1400;
			int y0 = -64;
			int y1 = 145;
			double slope = -0.593750;
			double normal = 1.684211;
			int nrPtsTotal = 0;
			int nrPtsInImage = 0;
			for (int x = x0; x <= x1; x++) {
				for (int y = y0; y <= y1; y++) {
					if (y + 1.0 - slope * (x + 1.0) <= bounds[0] && y + 1.0 - slope * (x + 1.0) >= bounds[1] &&
						y + 1.0 - normal * (x + 1.0) <= bounds[2] && y + 1.0 - normal * (x + 1.0) >= bounds[3]) {
						nrPtsTotal++;
						if (x >= 0 && x < cols && y >= 0 && y < rows) {
							nrPtsInImage++;
						}
					}
				}
			}

			Assert::AreEqual(1602, nrPtsTotal);
			Assert::AreEqual(1125, nrPtsInImage);
		}

		TEST_METHOD(TestBoolConversion) {
			bool test1 = true;
			bool test2 = false;
			bool test3 = true;
			int acVal = static_cast<int>(test1) + static_cast<int>(test2) + static_cast<int>(test3);
			Assert::AreEqual(2, acVal);
		}

	};
}