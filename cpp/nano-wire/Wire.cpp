#include "stdafx.h"

#include "Wire.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <fstream>
#include <iomanip>

#define PI 3.1415926536 

using namespace std;

Wire::Wire() {
}


Wire::~Wire() {
}

void Wire::setupParameters(const std::string filename, int ksize, const cv::Rect & roi, 
	int minLength, int maxLength, int stepSize, double maxDist, double ratio1, double ratio2, int barWidth, int barLength) {
	// load parameters
	m_ROI = roi;
	m_filename = filename;
	m_ksize = ksize;
	m_minLength = minLength;
	m_maxLength = maxLength;
	m_stepSize = stepSize;
	m_maxDist = maxDist;
	m_detectionRatio1 = ratio1;
	m_detectionRatio2 = ratio2;
	m_detectionBarWidth = barWidth;
	m_detectionBarLength = barLength;
}

void Wire::run() {
	// load image
	loadImage();

	// find the mask of in and out points
	findInOutPointMasks();

	// loop
	for (int row = m_stepSize - 1; row < m_binaryImage.rows - m_stepSize; row += m_stepSize) {
		// detect in and out points
		std::vector<cv::Point> inPtIdx;
		std::vector<cv::Point> outPtIdx;
		bool isOK = findInOutPointsInLine(row, inPtIdx, outPtIdx);
		if (!isOK) {
			cout << endl;
			continue;
		}

		// find valid local segments
		for (size_t k = 0; k < inPtIdx.size(); k++) {
			// calculate the local properties of the wire
			LocalWireProps props;
			findLocalWireProperties(inPtIdx[k].x, outPtIdx[k].x, row, props);

			// loop to velidate the properties
			if (!props.isWireHorizontal) {
				bool isPropValid[2] = { false, false };
				for (int index = 0; index < 2; index++) {
					if (props.angle[index] > 0.0 && props.validCheck[index]) {
						// check detection bar from A
						double ptx = static_cast<double>(inPtIdx[k].x + 1) + m_detectionBarWidth * cos(props.angle[index] - PI / 2.0);
						double pty = static_cast<double>(row) + m_detectionBarWidth * sin(props.angle[index] - PI / 2.0);
						cv::Point2d midPt(ptx, pty);						
						bool temp1 = checkLocalWireProperties(midPt, props, index);
					
						// check detection bar from B
						ptx = static_cast<double>(outPtIdx[k].x) + m_detectionBarWidth * cos(props.angle[index] + PI / 2.0);
						pty = static_cast<double>(row) + m_detectionBarWidth * sin(props.angle[index] + PI / 2.0);
						midPt = cv::Point2d(ptx, pty);						
						bool temp2 = checkLocalWireProperties(midPt, props, index);

						// check detection bar from the middle point of AB
						ptx = static_cast<double>(inPtIdx[k].x + outPtIdx[k].x) / 2.0 + 1.0;
						pty = static_cast<double>(row);
						midPt = cv::Point2d(ptx, pty);
						bool temp3 = checkLocalWireProperties(midPt, props, index);

						int sum = static_cast<int>(temp1) + static_cast<int>(temp2) + static_cast<int>(temp3);
						isPropValid[index] = (sum > 1 ? true : false);

					}
					if (props.angle[index] < 0.0 && props.validCheck[index]) {
						// check detection bar from A
						double ptx = static_cast<double>(inPtIdx[k].x + 1) + m_detectionBarWidth * cos(props.angle[index] + PI / 2.0);
						double pty = static_cast<double>(row) + m_detectionBarWidth * sin(props.angle[index] + PI / 2.0);
						cv::Point2d midPt(ptx, pty);
						bool temp1 = checkLocalWireProperties(midPt, props, index);

						// check detection bar from B
						ptx = static_cast<double>(outPtIdx[k].x) + m_detectionBarWidth * cos(props.angle[index] - PI / 2.0);
						pty = static_cast<double>(row) + m_detectionBarWidth * sin(props.angle[index] - PI / 2.0);
						midPt = cv::Point2d(ptx, pty);
						bool temp2 = checkLocalWireProperties(midPt, props, index);

						// check detection bar from the middle point of AB
						ptx = static_cast<double>(inPtIdx[k].x + outPtIdx[k].x) / 2.0 + 1.0;
						pty = static_cast<double>(row);
						midPt = cv::Point2d(ptx, pty);
						bool temp3 = checkLocalWireProperties(midPt, props, index);

						int sum = static_cast<int>(temp1) + static_cast<int>(temp2) + static_cast<int>(temp3);
						isPropValid[index] = (sum > 1 ? true : false);
					}
				}

				// determine the local width and slope of the wire
				double width = -1.0;
				double slope = 0.0;

				if (isPropValid[0] && isPropValid[1]) {
					width = props.width[0];
					slope = props.slope[0];
				}
				if (isPropValid[0] && !isPropValid[1]) {
					width = props.width[0];
					slope = props.slope[0];
				}
				if (!isPropValid[0] && isPropValid[1]) {
					width = props.width[1];
					slope = props.slope[1];
				}

				// merge local properties into a wire
				merge(inPtIdx[k].x, outPtIdx[k].x, row, width, slope);
			}
		}
	}
}

void Wire::visualizeResults() {
	m_resultImage = m_grayScaleImage.clone();
	cv::cvtColor(m_resultImage, m_resultImage, CV_GRAY2RGB);

	for (size_t j = 0; j < m_wires.size(); j++) {
		for (size_t k = 0; k < m_wires[j].size(); k++) {
			int ptX = static_cast<int>(m_wires[j][k].val[2] + m_offsetX);
			int ptY = static_cast<int>(m_wires[j][k].val[3] + m_offsetY);
			int r = static_cast<int>(m_wires[j][k].val[6] / 2.0);
			cv::circle(m_resultImage, cv::Point(ptX, ptY), r, CV_RGB(255, 0, 0), 2);
		}
	}
	cv::imwrite("ResultsCPP.jpg", m_resultImage);
}

void Wire::saveResults() {
	// define the name of the file
	const string filename = "ResultsCPP.txt";

	// check whether it exists
	ifstream fin(filename.c_str());
	if (fin) {
		remove(filename.c_str());
	}

	// write the results
	ofstream fout(filename.c_str());
	if (!fout) {
		cout << "Cannot open file " << endl;
		return;
	}
	for (size_t j = 0; j < m_wires.size(); j++) {
		fout << "Wire measurement: " << j + 1 << "." << endl;
		for (size_t k = 0; k < m_wires[j].size(); k++) {
			fout << fixed;
			fout << setprecision(6);
			fout << m_wires[j][k].val[2] + m_offsetX << "\t" << m_wires[j][k].val[3] + m_offsetY << "\t"
				 << m_wires[j][k].val[6] << endl;
		}		
	}
	fout.close();
}

void Wire::loadImage() {
	// load the original image
	cv::Mat image = cv::imread(m_filename, -1);

	// convert it to gray-scale if it has more than 3 channels
	if (image.channels() == 3) {
		cv::cvtColor(image, m_grayScaleImage, CV_RGB2GRAY);
	}
	else {
		m_grayScaleImage = image.clone();	// might not be very efficient
	}

	// get the thresold using otsu from the ROI
	cv::Mat tempBinaryImage;
	double threshold = cv::threshold(m_grayScaleImage(m_ROI), tempBinaryImage, 128, 255, cv::ThresholdTypes::THRESH_OTSU);

	// crop the image and threshold it
	int rows = m_grayScaleImage.rows;
	int cols = m_grayScaleImage.cols;
	int top = cvRound(static_cast<double>(rows) / 100.0);
	int bottom = cvRound(static_cast<double>(rows) / 10.0);
	int left = cvRound(static_cast<double>(cols) / 100.0);
	int right = cvRound(static_cast<double>(cols) / 100.0);

	m_offsetX = left;
	m_offsetY = top;
	cv::threshold(m_grayScaleImage(cv::Rect(left - 1, top - 1, cols - left - right + 1, rows - top - bottom + 1)), m_binaryImage, threshold, 255, cv::ThresholdTypes::THRESH_BINARY_INV);

	// blur the binary image
	cv::medianBlur(m_binaryImage, m_binaryImage, m_ksize);
}

void Wire::findInOutPointMasks() {
	// calculate the difference matrix
	cv::Mat kernel = (cv::Mat_<float>(1, 2) << -1.f, 1.f);	// kernel
	cv::Mat diffMat;	// difference matrix
	cv::filter2D(m_binaryImage, diffMat, CV_64FC1, kernel, cv::Point(0, 0), 0.0);

	// find the mask of in-points (0-->255)
	cv::compare(diffMat, 255.0, m_inPtMask, cv::CmpTypes::CMP_EQ);
	m_inPtMask = m_inPtMask(cv::Rect(0, 0, m_inPtMask.cols - 1, m_inPtMask.rows));

	// find the mask of out-points (255-->0)
	cv::compare(diffMat, -255.0, m_outPtMask, cv::CmpTypes::CMP_EQ);
	m_outPtMask = m_outPtMask(cv::Rect(0, 0, m_outPtMask.cols - 1, m_outPtMask.rows));
}

/// TODO: improve the performance
bool Wire::findInOutPointsInLine(int row, vector<cv::Point>& inPtIdx, vector<cv::Point>& outPtIdx) {

	// get the r-th row (line) of the in-point masks
	cv::Mat inPtMaskRow = m_inPtMask.row(row);
	cv::findNonZero(inPtMaskRow, inPtIdx);

	// get the r-th row (line) of the out-point masks
	cv::Mat outPtMaskRow = m_outPtMask.row(row);
	cv::findNonZero(outPtMaskRow, outPtIdx);

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;

	// remove the left boundary regions
	if (inPtIdx[0].x > outPtIdx[0].x) {
		outPtIdx.erase(outPtIdx.begin());
	}

	// remove the right boundary regions
	if (inPtIdx.back().x > outPtIdx.back().x) {
		inPtIdx.pop_back();
	}

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;

	// remove point pair whose distance is not within the range
	assert(inPtIdx.size() == outPtIdx.size());
	for (size_t k = 0; k < inPtIdx.size(); ) {
		int dist = outPtIdx[k].x - inPtIdx[k].x;
		if (dist <= m_minLength || dist >= m_maxLength) {
			// erase the invalid pair
			inPtIdx.erase(inPtIdx.begin() + k);
			outPtIdx.erase(outPtIdx.begin() + k);
		}
		else {
			k++;
		}
	}

	// return true if we cannot find any candidates
	if (inPtIdx.size() == 0 || outPtIdx.size() == 0)
		return false;
	else
		return true;
}

void Wire::findLocalWireProperties(int col1, int col2, int row, LocalWireProps& props) {
	// set up two thresholds
	int thresh1 = 20;
	int thresh2 = 10;

	// calculate the length of the line segment
	int segLength = col2 - col1 - 1;

	// find line segments AC
	cv::Mat tempSegACUpLine = m_binaryImage(cv::Rect(col1 + 1, 0, 1, row));
	cv::Mat segACUpLine;
	cv::flip(tempSegACUpLine, segACUpLine, 0);	// flip the upward line

	cv::Point segACUpMinLoc;
	double segACUpMinVal;
	cv::minMaxLoc(segACUpLine, &segACUpMinVal, 0, &segACUpMinLoc, 0);
	int segACUpIdx = segACUpMinLoc.y + 1;

	cv::Mat segACDownLine = m_binaryImage(cv::Rect(col1 + 1, row + 1, 1, m_binaryImage.rows - row - 1));
	double segACDownMinVal;
	cv::Point segACDownMinLoc;
	cv::minMaxLoc(segACDownLine, &segACDownMinVal, 0, &segACDownMinLoc, 0);
	int segACDownIdx = segACDownMinLoc.y + 1;

	// find the line segment BD
	cv::Mat tempSegBDUpLine = m_binaryImage(cv::Rect(col2, 0, 1, row));
	cv::Mat segBDUpLine;
	cv::flip(tempSegBDUpLine, segBDUpLine, 0);	// flip the upward line

	cv::Point segBDUpMinLoc;
	double segBDUpMinVal;
	cv::minMaxLoc(segBDUpLine, &segBDUpMinVal, 0, &segBDUpMinLoc, 0);
	int segBDUpIdx = segBDUpMinLoc.y + 1;

	cv::Mat segBDDownLine = m_binaryImage(cv::Rect(col2, row + 1, 1, m_binaryImage.rows - row - 1));
	double segBDDownMinVal;
	cv::Point segBDDownMinLoc;
	cv::minMaxLoc(segBDDownLine, &segBDDownMinVal, 0, &segBDDownMinLoc, 0);
	int segBDDownIdx = segBDDownMinLoc.y + 1;

	props.isWireHorizontal = true;
	// case 1: negative slope (bottom left --> top right)
	// This is a little bit conter-intuitive. However, in fact, as x increases, y decreases. Hence, the slope is negative.
	bool condition1 = segBDUpIdx >= thresh2 && segBDDownIdx <= thresh1 && segBDUpMinVal != 255.0;
	bool condition2 = segACDownIdx >= thresh2 && segACUpIdx <= thresh1 && segACDownMinVal != 255.0;
	if (condition1 && condition2) {
		double AC = static_cast<double>(segACDownIdx - 1);
		double BD = static_cast<double>(segBDUpIdx - 1);
		double AB = static_cast<double>(segLength);

		// update the slope
		props.slope[0] = -BD / AB;
		props.slope[1] = -AC / AB;

		// update the normal
		for (int k = 0; k < 2; k++) {
			if (props.slope[k] != 0.0) {
				props.normal[k] = -1.0 / props.slope[k];
			} else {
				props.validCheck[k] = false;
			}
		}

		// update the angle
		props.angle[0] = atan(props.slope[0]);
		props.angle[1] = atan(props.slope[1]);

		// update width
		props.width[0] = AB * fabs(sin(props.angle[0]));
		props.width[1] = AB * fabs(sin(props.angle[1]));

		// check width
		for (int k = 0; k < 2; k++) {
			if (props.width[k] > m_maxLength || props.width[k] < m_minLength) {
				props.validCheck[k] = false;
			}
		}	

		// update the flag for horizontal wire
		props.isWireHorizontal = false;
	}

	// case 2: positive slope (bottom right --> top left)
	bool condition3 = segBDDownIdx >= thresh2 && segBDUpIdx <= thresh1 && segBDDownMinVal != 255.0;
	bool condition4 = segACUpIdx >= thresh2 && segACDownIdx <= thresh1 && segACUpMinVal != 255.0;
	if (condition3 && condition4) {
		double AC = static_cast<double>(segACUpIdx - 1);
		double BD = static_cast<double>(segBDDownIdx - 1);
		double AB = static_cast<double>(segLength);

		// update the slope
		props.slope[0] = BD / AB;
		props.slope[1] = AC / AB;

		// update the normal
		for (int k = 0; k < 2; k++) {
			if (props.slope[k] != 0.0) {
				props.normal[k] = -1.0 / props.slope[k];
			}
			else {
				props.validCheck[k] = false;
				props.isWireHorizontal = true;
			}
		}

		// update the angle
		props.angle[0] = atan(props.slope[0]);
		props.angle[1] = atan(props.slope[1]);

		// update width
		props.width[0] = AB * fabs(sin(props.angle[0]));
		props.width[1] = AB * fabs(sin(props.angle[1]));

		// check width
		for (int k = 0; k < 2; k++) {
			if (props.width[k] > m_maxLength || props.width[k] < m_minLength) {
				props.validCheck[k] = false;
			}
		}

		// update the flag for horizontal wire
		props.isWireHorizontal = false;

	}
}

void Wire::calculateDetectionBarProperties(const cv::Point2d& midPt, const LocalWireProps& props, int index,
	cv::Point2d& farPt, cv::Point2d& nearPt, cv::Vec4d& bounds) {
	// calculate the up point
	double offsetX = m_detectionBarWidth * cos(props.angle[index] + PI / 2.0);
	double offsetY = m_detectionBarWidth * sin(props.angle[index] + PI / 2.0);
	cv::Point2d upPt = midPt + cv::Point2d(offsetX, offsetY);
	bounds.val[0] = upPt.y + 1.0 - props.slope[index] * (upPt.x + 1.0);

	// calculate the down point
	offsetX = m_detectionBarWidth * cos(props.angle[index] - PI / 2.0);
	offsetY = m_detectionBarWidth * sin(props.angle[index] - PI / 2.0);
	cv::Point2d downPt = midPt + cv::Point2d(offsetX, offsetY);
	bounds.val[1] = downPt.y + 1.0 - props.slope[index] * (downPt.x + 1.0);

	// calculate the far point
	if (props.slope[index] > 0.0) {
		offsetX = m_detectionBarLength * cos(props.angle[index]);
		offsetY = m_detectionBarLength * sin(props.angle[index]);
	} else {
		offsetX = m_detectionBarLength * cos(props.angle[index] + PI);
		offsetY = m_detectionBarLength * sin(props.angle[index] + PI);
	}	
	farPt = midPt + cv::Point2d(offsetX, offsetY);
	bounds.val[2] = farPt.y + 1.0 - props.normal[index] * (farPt.x + 1.0);

	// calculate the near point
	if (props.slope[index] > 0.0) {
		offsetX = m_detectionBarLength * cos(props.angle[index] + PI);
		offsetY = m_detectionBarLength * sin(props.angle[index] + PI);
	}
	else {
		offsetX = m_detectionBarLength * cos(props.angle[index]);
		offsetY = m_detectionBarLength * sin(props.angle[index]);
	}
	nearPt = midPt + cv::Point2d(offsetX, offsetY);
	bounds.val[3] = nearPt.y + 1.0 - props.normal[index] * (nearPt.x + 1.0);
}

bool Wire::checkLocalWireProperties(const cv::Point2d& midPt, const LocalWireProps& props, int index) {
	// calculate detection bar properties
	cv::Point2d farPt;
	cv::Point2d nearPt;
	cv::Vec4d bounds;
	calculateDetectionBarProperties(midPt, props, index, farPt, nearPt, bounds);

	// define counting variables
	int nrPtsInImage = 0;
	int nrPtsTotal = 0;

	// define a vector containing all points
	vector<cv::Point> ptsInImage;

	// calculate the lower and upper bounds in two cases
	int x0;
	int x1;
	if (props.slope[index] > 0.0) {
		x0 = cvFloor(nearPt.x) - cvCeil(m_detectionBarWidth);
		x1 = cvCeil(farPt.x) + cvCeil(m_detectionBarWidth);
	}
	else {
		x0 = cvFloor(farPt.x) - cvCeil(m_detectionBarWidth);
		x1 = cvCeil(nearPt.x) + cvCeil(m_detectionBarWidth);
	}

	int y0 = cvFloor(nearPt.y) - cvCeil(m_detectionBarWidth);
	int y1 = cvCeil(farPt.y) + cvCeil(m_detectionBarWidth);

	// loop
	int rows = m_binaryImage.rows;
	int cols = m_binaryImage.cols;
	for (int x = x0; x <= x1; x++) {
		for (int y = y0; y <= y1; y++) {
			if (y + 1.0 - props.slope[index] * (x + 1.0) <= bounds.val[0] && y + 1.0 - props.slope[index] * (x + 1.0) >= bounds.val[1] &&
				y + 1.0 - props.normal[index] * (x + 1.0) <= bounds.val[2] && y + 1.0 - props.normal[index] * (x + 1.0) >= bounds.val[3]) {
				nrPtsTotal++;
				if (x >= 0 && x < cols && y >= 0 && y < rows) {
					nrPtsInImage++;
					ptsInImage.push_back(cv::Point(x, y));
				}
			}
		}
	}

	// check the ratio
	if (static_cast<double>(nrPtsInImage) / static_cast<double>(nrPtsTotal) > m_detectionRatio1) {
		double nrPtsInWire = 0.0;
		for (size_t k = 0; k < ptsInImage.size(); k++) {
			nrPtsInWire += (static_cast<double>(m_binaryImage.at<uchar>(ptsInImage[k].y, ptsInImage[k].x)) / 255.0);
		}
		if (nrPtsInWire / static_cast<double>(nrPtsInImage) < m_detectionRatio2) {
			return false;
		} else {
			return true;
		}
	} else {
		return false;
	}
	
}

void Wire::merge(int col1, int col2, int row, double width, double slope) {
	// ignore the property whose width is negative
	if (width < 0.0) {
		return;
	}

	bool isPropNew = true;
	if (m_wires.size() != 0) {
		double minDist = 1e4;
		size_t minIdx = 0;
		double minDist1;
		double minDist2;
		for (size_t k = 0; k < m_wires.size(); k++) {
			// find the maxmimum row index associated with one candidate
			cv::Mat tempMat(m_wires[k]);
			vector<cv::Mat> tempMatVec;
			cv::split(tempMat, tempMatVec);
			cv::Point maxLoc;
			cv::minMaxLoc(tempMatVec[3], 0, 0, 0, &maxLoc);
			int index = maxLoc.y;

			// calculate 2 distances
			double midPtX2 = m_wires[k][index].val[2];
			double y2 = m_wires[k][index].val[3];
			double slope2 = m_wires[k][index].val[4];
			double aux2 = m_wires[k][index].val[5];

			double midPtX = static_cast<double>(col1 + 1 + col2) / 2.0;
			double y = static_cast<double>(row);
			double aux = y + 1.0 - slope * (midPtX + 1.0);

			double dist1 = fabs(slope2 * (midPtX + 1.0) + aux2 - y - 1.0) / sqrt(1.0 + slope2 * slope2);
			double dist2 = fabs(slope * (midPtX2 + 1.0) + aux - y2 - 1.0) / sqrt(1.0 + slope * slope);
			double dist = dist1 + dist2;

			if (dist < minDist) {
				minIdx = k;
				minDist = dist;
				minDist1 = dist1;
				minDist2 = dist2;
			}
		}

		// merge
		//cout << fixed << setprecision(6) << "minDist1: " << minDist1 << " minDist2: " << minDist2 << "." << endl;
		if (minDist1 < m_maxDist && minDist2 < m_maxDist) {
			double inPtX3 = static_cast<double>(col1 + 1);
			double outPtX3 = static_cast<double>(col2);
			double midPtX3 = static_cast<double>(col1 + 1 + col2) / 2.0;
			double y3 = static_cast<double>(row);
			double aux3 = y3 + 1.0 - (midPtX3 + 1.0) * slope;
			Vec7d tempProp(inPtX3, outPtX3, midPtX3, y3, slope, aux3, width);
			m_wires[minIdx].push_back(tempProp);
			isPropNew = false;
		}
	} else {
		isPropNew = true;
	}

	// insert a new property if it is new
	if (isPropNew) {
		vector<Vec7d> tempVec;
		double inPtX3 = static_cast<double>(col1 + 1);
		double outPtX3 = static_cast<double>(col2);
		double midPtX3 = static_cast<double>(col1 + 1 + col2) / 2.0;
		double y3 = static_cast<double>(row);
		double aux3 = y3 + 1.0 - (midPtX3 + 1.0) * slope;
		Vec7d tempProp(inPtX3, outPtX3, midPtX3, y3, slope, aux3, width);
		tempVec.push_back(tempProp);
		m_wires.push_back(tempVec);
	}
}
