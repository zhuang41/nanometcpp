#include "stdafx.h"

#include "Wire.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	// define parameters with default values
	string filename = "C:/Users/Yao/Documents/Data/nano/wire/1.jpg";
	int ksize = 7;
	cv::Rect roi(1535, 0, 2155 - 1535, 512);

	int minLength = 5;
	int maxLength = 400;

	int stepSize = 40;
	double maxDist = 30.0;

	double ratio1 = 0.75;
	double ratio2 = 0.75;

	int barWidth = 2;
	int barLength = 200;

	double resizeFactor = -1.0;

	// define class instance
	Wire wire;

	// set up parameters
	wire.setupParameters(filename, ksize, roi, minLength, maxLength, 
		stepSize, maxDist, ratio1, ratio2, barWidth, barLength);

	// run
	wire.run();

	// save results
	wire.saveResults();

	// visualize results;
	wire.visualizeResults();

	return 0;
}