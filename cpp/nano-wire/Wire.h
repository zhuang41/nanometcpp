#pragma once

#include <opencv2/core/core.hpp>

class Wire {
public:
	/* @brief	default constructor 
	 */
	Wire();

	/* @brief	default deconstructor
	 */
	~Wire();

	/*
	 * @brief		initialize parameters
	 * @param[in]	filename: the full filename of the image file
	 * @param[in]	ksize: the size of the median filter
	 * @param[in]	minLength: the minimum length of a valid line segment linking a pair of in and out points
	 * @param[in]	maxLength: the maximum length of a valid line segment linking a pair of in and out points
	 * @param[in]	minUpIdx: the minimum upward length of a verticle chord
	 * @param[in]	minDownIdx: the minimum downward length of a verticle chord
	 * @param[in]	stepsize: the step size of the sampling in rows
	 * @param[in]	maxDist: the threshold used for circle merging
	 * @param[in]	ratio1: the threshold used in validate the local property of the wire
	 * @param[in]	ratio2: the threshold used in validate the local property of the wire
	 * @param[in]	barWidth: the threshold of the bar width
	 * @param[in]	barLength: the threshold of the bar length
	 */
	void setupParameters(const std::string filename, int ksize, const cv::Rect& roi, 
		int minLength, int maxLength, int stepSize, double maxDist, double ratio1, double ratio2, int barWidth, int barLength);

	/*
	 * @brief	run the whole pipeline
	 * @output	the private member m_circles
	 */
	void run();

	/*
	* @brief	visualize the results
	*/
	void visualizeResults();

	/*
	* @brief	save the final circles
	*/
	void saveResults();

	//< local wire properties
	typedef struct {
		bool validCheck[2] = { true, true };

		bool isWireHorizontal = false;

		double slope[2] = { 0.0, 0.0 };
		double angle[2] = { 0.0, 0.0 };
		double width[2] = { -1.0, -1.0 };
		double normal[2] = { 0.0, 0.0 };	
	} LocalWireProps;

	//< define a new type of 7-element vector in OpenCV
	typedef cv::Vec<double, 7> Vec7d;

private:
	/*
	 * @brief	1. load the original image I by the filename
	 * 			2. covert I into a grayscale image I_g
	 * 			3. obtain the binary image I_b using otsu
	 * 			4. remove noise from I_b using a median filter
	 * @input	the private member m_filename
	 * @output	the private member m_binaryImage
	 */
	void loadImage();

	/*
	 * @brief	find the masks of the in-points and out-points using a 1-D filter [-1, 1], respectively
	 * @input	the private member m_binaryImage
	 * @output	the private member m_inPtMask
	 * @output	the private member m_outPtMask
	 */
	void findInOutPointMasks();

	/*
	 * @brief			find the in and out points of a scan line (row) of the binary image
	 * @param[in]		row: the row index of the scan line
	 * @param[in, out]  inPtIdx: the column indices of the in points
	 * @param[in, out]	outPtIdx: the column indices of the out points
	 * @note			The size of inPtIdx and that of outPtIdx are the same.
	 * @return			true if the size is not 0; false otherwise
	 */
	bool findInOutPointsInLine(int row, std::vector<cv::Point>& inPtIdx, std::vector<cv::Point>& outPtIdx);

	/*
	 * @brief			find the local properties of the wire
	 * @param[in]		col1: the column index of the in point (start point)
	 * @param[in]		col2: the column index of the out point (end point)
	 * @param[in]		row: the row index
	 * @param[in, out]	the local properties
	 */
	void findLocalWireProperties(int col1, int col2, int row, LocalWireProps& props);

	/*
	 * @brief			calculate the properties of the detection bar
	 * @param[in]		midPt: middle point 
	 * @param[in]		props: the local properties
	 * @param[in]		index: in-0; out-1
	 * @param[in, out]	farPt: the "farther" point of the bounding box
	 * @param[in, out]	nearPt: the "nearer" point of the bounding box
	 * @param[in, out]	bounds: the bounds used to check whether a point is in the bounding box
	 */
	void calculateDetectionBarProperties(const cv::Point2d& midPt, const LocalWireProps& props, int index,
		cv::Point2d& farPt, cv::Point2d& nearPt, cv::Vec4d& bounds);

	/*
	 * @brief		calculate the properties of the detection bar
	 * @param[in]	midPt: middle point
	 * @param[in]	props: the local properties
	 * @param[in]	index: in-0; out-1
	 * @return		true if it is valid
	 */
	bool checkLocalWireProperties(const cv::Point2d& midPt, const LocalWireProps& props, int index);

	/*
	 * @brief		calculate the properties of the detection bar
	 * @param[in]	col1: the column index of the in point (start point)
	 * @param[in]	col2: the column index of the out point (end point)
	 * @param[in]	row: the row index
	 * @param[in]	width: the width of the wire measured at this location
	 * @param[in]	slope: the slope of the wire measured at this location
	 */
	void merge(int col1, int col2, int row, double width, double slope);


	cv::Mat m_grayScaleImage;	//< gray-scale image
	cv::Mat m_binaryImage;		//< binary image
	cv::Mat m_resultImage;		//< result image

	int m_offsetX;	// offset in X
	int m_offsetY;	// offset in Y

	cv::Mat m_inPtMask;		//< mask of in points
	cv::Mat m_outPtMask;	//< mask of out points

	std::string m_filename;	//< full filename of the original image

	int m_ksize;		//< size of the median filter		
	cv::Rect m_ROI;		//< the ROI for thresholding

	int m_stepSize;		//< the step size of the sampling in rows

	int m_minLength;	//< the minimum length of a valid line segment linking a pair of in and out points
	int m_maxLength;	//< the maximum length of a valid line segment linking a pair of in and out points

	double m_maxDist;	//< the threshold used for circle merging  

	double m_detectionRatio1;	//< the threshold used in validate the local property of the wire
	double m_detectionRatio2;	//< the threshold used in validate the local property of the wire

	int m_detectionBarWidth;	//< the threshold of the bar width
	int m_detectionBarLength;	//< the threshold of the bar length

	std::vector<std::vector<Vec7d> > m_wires;	//< all wires: every wire consists of several local middle points and widths
};

