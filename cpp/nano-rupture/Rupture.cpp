#include "stdafx.h"

#include "Rupture.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

Rupture::Rupture() {
}


Rupture::~Rupture() {
}

void Rupture::setupParameters(const std::string filename, int ksize, const cv::Rect & roi, int threshold, double minArea) {
	m_filename = filename;
	m_ROI = roi;
	m_filename = filename;
	m_ksize = ksize;
	m_threshold = threshold;
	m_minArea = minArea;
}

void Rupture::run(bool useContours) {
	loadImage();
	if (useContours) {
		detectRegions();
	} else {
		detectRegions(8);
	}

}

void Rupture::visualizeResults() {
	// get the image for display
	cv::cvtColor(m_grayScaleImage, m_resultImage, CV_GRAY2RGB);

	// draw the regions
	if (m_labels.rows == 0 ||& m_labels.cols == 0) {
		for (size_t k = 0; k < m_regionProps.size(); k++) {
			// draw region
			vector<vector<cv::Point> > tempContours;
			tempContours.push_back(m_regionProps[k].points);
			cv::drawContours(m_resultImage, tempContours, -1, CV_RGB(255, 0, 0), -1);
		}
	} else {
		for (int r = 0; r < m_labels.rows; r++) {
			for (int c = 0; c < m_labels.cols; c++) {
				if (m_labels.at<int>(r, c) != 0) {
					m_resultImage.at<cv::Vec3b>(r + m_offsetY, c + m_offsetY) = cv::Vec3b(0, 0, 255);
				}
			}
		}
	}

	// put text
	for (size_t k = 0; k < m_regionProps.size(); k++) {
		string text = to_string(static_cast<int>(m_regionProps[k].area));	// in C++11
		cv::Point origin = cv::Point(static_cast<int>(m_regionProps[k].cx),
			static_cast<int>(m_regionProps[k].cy));
		cv::putText(m_resultImage, text, origin, cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 0, 0));
	}

	// save the image
	cv::imwrite("ResultsCPP.jpg", m_resultImage);
}

void Rupture::saveResults() {
	// define the name of the file
	const string filename = "ResultsCPP.txt";

	// check whether it exists
	ifstream fin(filename.c_str());
	if (fin) {
		remove(filename.c_str());
	}

	// write the results
	ofstream fout(filename.c_str());
	if (!fout) {
		cout << "Cannot open file " << endl;
		return;
	}
	for (size_t j = 0; j < m_regionProps.size(); j++) {
		fout << "Region: " << j + 1 << "." << endl;
		fout << fixed << setprecision(6) << "Area: " << m_regionProps[j].area << "." << endl;
		fout << fixed << setprecision(6) << "cx: " << m_regionProps[j].cx << "." << endl;
		fout << fixed << setprecision(6) << "cy: " << m_regionProps[j].cy << "." << endl;
	}
	fout.close();
}

void Rupture::loadImage() {
	// load the original image
	cv::Mat image = cv::imread(m_filename, -1);

	// convert it to gray-scale if it has more than 3 channels
	if (image.channels() == 3) {
		cv::cvtColor(image, m_grayScaleImage, CV_RGB2GRAY);
	}
	else {
		m_grayScaleImage = image.clone();	// might not be very efficient
	}

	// get the thresold using otsu from the ROI
	cv::Mat tempBinaryImage;
	cv::threshold(m_grayScaleImage(m_ROI), m_binaryImage, m_threshold, 255, cv::ThresholdTypes::THRESH_BINARY_INV);

	// calculate the offset
	m_offsetX = m_ROI.x;
	m_offsetY = m_ROI.y;

	// blur the binary image
	cv::medianBlur(m_binaryImage, m_binaryImage, m_ksize);
}

void Rupture::detectRegions() {
	// make a deep copy of the binary image
	cv::Mat tempBinaryImage = m_binaryImage.clone();

	// find the contours
	vector<vector<cv::Point> > tempContours;
	vector<cv::Vec4i> hierarchy;
	cv::findContours(tempBinaryImage, tempContours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, cv::Point(m_offsetX, m_offsetY));

	// remove holes using the hierarchy relationship
	vector<vector<cv::Point> > contours;
	assert(hierarchy.size() == tempContours.size());
	for (size_t cIdx = 0; cIdx < tempContours.size(); cIdx++) {
		if (hierarchy[cIdx].val[2] == -1 && hierarchy[cIdx].val[3] != -1) {
			continue;
		}
		contours.push_back(tempContours[cIdx]);
	}

	// calculate the properties
	for (size_t cIdx = 0; cIdx < contours.size(); cIdx++) {
		// moments
		cv::Moments moms = cv::moments(cv::Mat(contours[cIdx]));

		// discard too small regions
		double area = moms.m00;
		if (area < m_minArea) {
			continue;
		}

		// calculate the center of the region
		double cx = moms.m10 / area;
		double cy = moms.m01 / area;

		// get the valid rupture
		RegionProps props;
		props.area = area;
		props.cx = cx;
		props.cy = cy;
		props.points.insert(props.points.end(), contours[cIdx].begin(), contours[cIdx].end());

		// update the vector
		m_regionProps.push_back(props);
	}
}

void Rupture::detectRegions(int connectivity) {
	// calculate the connected regions
	cv::Mat stats;
	cv::Mat centroids;
	int nrLabels = cv::connectedComponentsWithStats(m_binaryImage, m_labels, stats, centroids, connectivity, CV_32S);

	// skip the background by starting from the second region
	for (int k = 1; k < nrLabels; k++) {
		if (stats.at<int>(k, 4) < m_minArea) {
			continue;
		}
		RegionProps props;
		props.area = stats.at<int>(k, 4);
		props.cx = centroids.at<double>(k, 0);
		props.cy = centroids.at<double>(k, 1);
		m_regionProps.push_back(props);
	}
}

