// nano-rupture.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Rupture.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;

int main(int argc, char *argv[]) {
	// define parameters with default values
	string filename = "C:/Users/Yao/Documents/Data/nano/rupture/1.tif";
	int ksize = 7;
	cv::Rect roi(0, 0, 1024, 943-60);
	int threshold = static_cast<int>(0.38 * 255.0);
	double minArea = 82;

	// define class instance
	Rupture rupture;

	// set up parameters
	rupture.setupParameters(filename, ksize, roi, threshold, minArea);
	
	// run
	rupture.run(false);

	// save results
	rupture.saveResults();

	// visualize results;
	rupture.visualizeResults();

	return 0;
}
