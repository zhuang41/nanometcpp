#pragma once

#include <opencv2/core/core.hpp>

class Rupture {
public:
	Rupture();
	~Rupture();

	/*
	 * @brief		initialize parameters
	 * @param[in]	filename: the full filename of the image file
	 * @param[in]	ksize: the size of the median filter
	 * @param[in]	roi: the region that we want to preserve
	 * @param[in]	threshold: the manually selected threshold for generating the binary image
	 */
	void setupParameters(const std::string filename, int ksize, const cv::Rect& roi, int threshold, double minArea);

	/*
	 * @brief		run the whole pipeline
	 * @param[in]	useContours: true for using contour-based method; false for using connected region based method
	 */
	void run(bool useContours);

	/*
	 * @brief	visualize the results
	 */
	void visualizeResults();

	/*
	 * @brief	save the final circles
	 */
	void saveResults();

	//< define a new type of region properties
	typedef struct {
		double cx = 0.0;
		double cy = 0.0;
		double area = 0.0;
		std::vector<cv::Point> points;
	} RegionProps;


private:
	/*
	 * @brief	1. load the original image I by the filename
	 * 			2. covert I into a grayscale image I_g
	 * 			3. obtain the binary image I_b using otsu
	 * 			4. remove noise from I_b using a median filter
	 * @input	the private member m_filename
	 * @output	the private member m_binaryImage
	 */
	void loadImage();

	/*
	 * @brief	detect useful regions in the image using contours
	 */
	void detectRegions();

	/*
	 * @brief		detect useful regions in the image using connected regions
	 * @param[in]	connectivity: the connectivity used to detect connected regions (4 or 8)
	 */
	void detectRegions(int connectivity);

	cv::Mat m_grayScaleImage;	//< gray-scale image
	cv::Mat m_binaryImage;		//< binary image
	cv::Mat m_resultImage;		//< result image

	cv::Mat m_labels;	//< all labels if using connectivity map

	int m_offsetX;	// offset in X
	int m_offsetY;	// offset in Y

	std::string m_filename;	//< full filename of the original image

	int m_ksize;		//< size of the median filter		
	cv::Rect m_ROI;		//< the ROI for thresholding
	int m_threshold;	//< the thereshold

	double m_minArea;	//< the minimum area we accept

	std::vector<RegionProps> m_regionProps;	//< all region props 

};

