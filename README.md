# nano
1. Prerequisites: redistributable for Visual Studio 2015 (Not tested yet) or installed Visual Studio 2015
2. You can directly run /matlab/nano-particle/run.m to see the results. Please change the path of the image and the path of the executable before you run it. Absolute paths are expected to be used. The executable is located at /cpp/x64/Debug/.
3. The resultsXXX.txt files in the path /matlab/nano-particle/ include particle information (center, radius and confidence) for C++ and MATLAB.
