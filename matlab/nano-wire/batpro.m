function batpro(imagename,step,ratio)
%you should read the comments with the report I sent to you, especially
%with Fig. 1
warning off all

% imagename='Gold.jpg';
% step=40;
% ratio=7.6;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x=imread(imagename);%read image

[xbinary, head, left]= resize_convert_pic(x);
%xbinary = rot90(xbinary);%%rotate the image 90 degree
imshow(x);
hold on
% imwrite(xbinary,'filename.jpg')
[row1 col1] = size(xbinary);

%%%%%%%%%%%%% For some difficult images, the two parameters will help eliminate too thick
%%%%%%%%%%%%% or too thin candidate measurements. For general propose, you
%%%%%%%%%%%%% should use big max_pix_thres and small min_pix_thres.
max_pix_thres=400;%maximum wire diameter in pix number
min_pix_thres=5;%minimum wire diameter in pix number

minlength = min_pix_thres;%maximum wire diameter in pix number
maxlength = max_pix_thres;%minimum wire diameter in pix number
detectlen = 200; %detection bar length
detectradius = 2;%detection bar width


close_threshold = 5;%if two wires are too close, then skip
inpic2totalcount = 0.75;%ratio of pix in the detection bar and pix actually in the image(detection bar may stick out of the image)
numinwire2inpic = 0.8;%ratio of pix in the wire and pix actually in the detection bar
numinwire2inpic_check = 0.75;%tighter threshold
% same_wire_thres = 5*pi/180;
max_dist = 30;%max distance for two measurement to be considered as in a single wire
pointinrange = zeros(8*detectradius*detectlen,2);%for plot debug information

wirecheck = zeros(1000,1000,7);%store the needed information for each measurement 
wirenum = 0;
toplot = 0;
for y_coordinate = step:step:row1-step

    line = xbinary(y_coordinate,:);%get a line of pix
    [inwire, outwire, goon]= find_in_out(line, minlength, maxlength);
    if (goon == 0)
       continue; 
    end
    for i = 1:length(inwire)
            
        validcheck = [1 1];
        hori = 0;%indicate whether the wire is horizontal, for now, we don't deal with horizontal ones, but if you want to improve the software in your version, you can do that by rotate the image by 90 degree.
        k = [0 0];%slope of the wire
        kwidth=[0 0];%perpendicular to k
        wirelength = [-1 -1];%see later comments
        wiretheta=0;
        [validcheck, hori, k, kwidth, wirelength, wiretheta] = get_diameter(xbinary, inwire(i), outwire(i), y_coordinate, min_pix_thres, max_pix_thres);
        if hori==0
            continue;%as I mentioned in the above comments, if the wire is horizontal we don't deal with it now, and we can deal with                      
             %it by rotating the image.
        end
        inrate = [0 0];%pix in wire/pix in detection bar
        wirek = 0;%see later comments
        wirewidth = -1;%see later comments
         %%%$ section 1 ends %%%%
         %% check whether the measure is valid when the angle of the wire is 0~pi/2 %%
         if hori==1     
            for j = 1:2
                if k(j)>0 && validcheck(j)==1
                    %the idea here is to stick out 3 dection bars (from A,
                    %B and the middle point of AB). A valid measurement
                    %should grantee that the ratio of (pix in wire)/(pix in
                    %detection bar) is a good one. Here (pix in wire)
                    %actually means pix in wire and also in detection bar.
                    %So the ratio is between 0 and 1
                    
                    %%%% section 2 begins %%%%
                    %% section 2 is to contruct the detection bar, the mid, down, far, near are the four sides of the rectangle dectionbar
                    
                    %% detectoin bar from A
                    mid = [inwire(i)+1+ detectradius*cos(wiretheta(j)-pi/2) y_coordinate+detectradius*sin(wiretheta(j)-pi/2)];                          
                    up = [mid(1)+ detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                    upbound = up(2)-k(j)*up(1);
                    down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                    downbound = down(2)-k(j)*down(1);
                    far = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                    farbound = far(2)-kwidth(j)*far(1);
                    near = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];
                    nearbound = near(2)-kwidth(j)*near(1);
                    %%%% section 2 ends %%%%
                    totalcount = 0;
                    inpic =0;
                    %%%% section 3 begins %%%%
                    [pointinrange, totalcount, inpic] = pix_in_range(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));

                    %%section 3 is to check the ratio of (pix in wire)/(pix in detection bar)
                    validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);

                    %%%% section 3 ends %%%%
                    %% detectoin bar from B
                    mid = [outwire(i)+ detectradius*cos(wiretheta(j)+pi/2) y_coordinate+detectradius*sin(wiretheta(j)+pi/2)];

                    up = [mid(1)+ detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                    upbound = up(2)-k(j)*up(1);
                    down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                    downbound = down(2)-k(j)*down(1);
                    far = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                    farbound = far(2)-kwidth(j)*far(1);
                    near = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];
                    nearbound = near(2)-kwidth(j)*near(1);

                    totalcount = 0;
                    inpic =0;

                    [pointinrange, totalcount, inpic] = pix_in_range(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));
                    validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);

                    %% detectoin bar from middle of AB
                    mid = [(outwire(i) + inwire(i))/2+1 y_coordinate];
                    up = [mid(1)+ detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                    upbound = up(2)-k(j)*up(1);
                    down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                    downbound = down(2)-k(j)*down(1);
                    far = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                    farbound = far(2)-kwidth(j)*far(1);
                    near = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];
                    nearbound = near(2)-kwidth(j)*near(1);

                    totalcount = 0;
                    inpic =0;

                    [pointinrange, totalcount, inpic] = pix_in_range(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));
                    validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);

                end

                %% pi/2~pi %%
                if k(j)<0 && validcheck(j)==1           

                    mid = [inwire(i)+1 + detectradius*cos(wiretheta(j)+pi/2) y_coordinate+detectradius*sin(wiretheta(j)+pi/2)];

                        up = [mid(1) + detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                        upbound = up(2)-k(j)*up(1);
                        down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                        downbound = down(2)-k(j)*down(1);
                        far = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];               
                        farbound = far(2)-kwidth(j)*far(1);
                        near = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                        nearbound = near(2)-kwidth(j)*near(1);
                        totalcount = 0;
                        inpic =0;

                        [pointinrange, totalcount, inpic] = pix_in_range_neg(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));
                        validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);

                        %%%% the other side
                        mid = [outwire(i) + detectradius*cos(wiretheta(j)-pi/2) y_coordinate+detectradius*sin(wiretheta(j)-pi/2)];
                        up = [mid(1) + detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                        upbound = up(2)-k(j)*up(1);
                        down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                        downbound = down(2)-k(j)*down(1);
                        far = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];               
                        farbound = far(2)-kwidth(j)*far(1);
                        near = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                        nearbound = near(2)-kwidth(j)*near(1);
                        totalcount = 0;
                        inpic =0;

                        [pointinrange, totalcount, inpic] = pix_in_range_neg(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));
                        validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);
                        %%%% the middle
                        mid = [(outwire(i) + inwire(i))/2+1 y_coordinate];
                        up = [mid(1) + detectradius*cos(wiretheta(j)+pi/2) mid(2)+detectradius*sin(wiretheta(j)+pi/2)];
                        upbound = up(2)-k(j)*up(1);
                        down = [mid(1) + detectradius*cos(wiretheta(j)-pi/2) mid(2)+detectradius*sin(wiretheta(j)-pi/2)];
                        downbound = down(2)-k(j)*down(1);
                        far = [mid(1) + detectlen*cos(wiretheta(j)+pi) mid(2)+detectlen*sin(wiretheta(j)+pi)];               
                        farbound = far(2)-kwidth(j)*far(1);
                        near = [mid(1) + detectlen*cos(wiretheta(j)) mid(2)+detectlen*sin(wiretheta(j))];
                        nearbound = near(2)-kwidth(j)*near(1);
                        totalcount = 0;
                        inpic =0;

                        [pointinrange, totalcount, inpic] = pix_in_range_neg(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k(j),kwidth(j));
                        validcheck(j) = pix_in_wire_ratio(xbinary,pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check);
                  
                end
            end
            %% the following 3 "if" to choose which side's measurement (A and B) to trust
            if validcheck(1)==1 && validcheck(2)==1
                [C I] = max(inrate);
                wirewidth = wirelength(I);
                wirek = k(I);
            end
            if validcheck(1)==1 && validcheck(2)==0
                wirewidth = wirelength(1);
                wirek = k(1);
            end
            if validcheck(1)==0 && validcheck(2)==1
                wirewidth = wirelength(2);
                wirek = k(2);
            end
         end
         % the codes within the following "if", is to determine whether a
         % meansure is belonging to a new wire or computed wire
         
         
         [wirecheck wirenum] = add_wirecheck(wirecheck, wirenum, wirewidth, wirek, inwire, outwire, y_coordinate,i,max_dist);
%          DrawCircle((inwire(i)+outwire(i))/2, y_coordinate, wirewidth/2, 32, 'r-');
%          if wirewidth ~= -1 
%             foundnew = 1;
%             if wirenum ~=0
%                 min_dist=10000;
%                 min_ind = 0;
%                 for wire_i=1:wirenum
%                     [curr_num curr_ptr]=max(wirecheck(wire_i,:,4));                  
%                     dist1 = abs(wirecheck(wire_i,curr_ptr,5)*(inwire(i)+outwire(i)+1)/2+wirecheck(wire_i,curr_ptr,6)-y_coordinate)/sqrt(1+wirecheck(wire_i,curr_ptr,5)^2);
%                     dist2 = abs(wirek*wirecheck(wire_i,curr_ptr,3)+(y_coordinate - wirek*(inwire(i)+outwire(i)+1)/2)-wirecheck(wire_i,curr_ptr,4))/sqrt(1+wirek^2);
%                     %dist1 is the distance between the middle of AB and the
%                     %line generated by existing wire's last measurement
%                     
%                     %dist2 is the distance between the existing wire's last
%                     %measurement middle point and the line generated by the
%                     %current measurement
%                     dist = dist1+dist2;
%                     if dist<min_dist
%                        min_dist=dist;
%                        min_ind = wire_i;
%                     end
%                 end
%                 wire_i=min_ind;
%                 [curr_num curr_ptr]=max(wirecheck(wire_i,:,4));                  
%                 dist1 = abs(wirecheck(wire_i,curr_ptr,5)*(inwire(i)+outwire(i)+1)/2+wirecheck(wire_i,curr_ptr,6)-y_coordinate)/sqrt(1+wirecheck(wire_i,curr_ptr,5)^2);
%                 dist2 = abs(wirek*wirecheck(wire_i,curr_ptr,3)+(y_coordinate - wirek*(inwire(i)+outwire(i)+1)/2)-wirecheck(wire_i,curr_ptr,4))/sqrt(1+wirek^2);
%                 if dist1<max_dist && dist2<max_dist
%                     wirecheck(wire_i,curr_ptr+1,1)= inwire(i)+1;%store the inwire point
%                     wirecheck(wire_i,curr_ptr+1,2)= outwire(i);%store the outwire point
%                     wirecheck(wire_i,curr_ptr+1,3)= (inwire(i)+1+outwire(i))/2;%store the mid point
%                     wirecheck(wire_i,curr_ptr+1,4)= y_coordinate;%store the y coordinate
%                     wirecheck(wire_i,curr_ptr+1,5)= wirek; %store the slope
%                     wirecheck(wire_i,curr_ptr+1,6)= y_coordinate-(inwire(i)+outwire(i)+1)/2*wirek;%this is for convenience of later computation
%                     wirecheck(wire_i,curr_ptr+1,7)= wirewidth;  %store the diameter               
%                     foundnew = 0;
%                 end
%                 
%             else             
%                 foundnew = 1;
%             end
%             if foundnew == 1
%                 wirenum=wirenum+1;
%                 wirecheck(wirenum,1,1)=inwire(i)+1;
%                 wirecheck(wirenum,1,2)=outwire(i);
%                 wirecheck(wirenum,1,3)=(inwire(i)+1+outwire(i))/2;
%                 wirecheck(wirenum,1,4)=y_coordinate; 
%                 wirecheck(wirenum,1,5)=wirek;
%                 wirecheck(wirenum,1,6)=y_coordinate-(inwire(i)+outwire(i)+1)/2*wirek;
%                 wirecheck(wirenum,1,7)=wirewidth;                        
%             end
%          end  
    end
end

if ~exist('detail','dir')
    mkdir('detail');
end
matname = 'detail/Results.mat';
if exist(matname,'file')
   delete(matname) 
end

firstcol = wirecheck(:,1,1);
firstcol = firstcol(firstcol~=0);
rownum = length(firstcol);

%%the following is to save the results specified by Phil
txtname = 'ResultsMATLAB.txt';
if exist(txtname,'file')
   delete(txtname);
end
fileID = fopen(txtname, 'w');
for i = 1:rownum
    xx = wirecheck(i,:,3)';
    xx = xx(xx~=0)+left;
    yy = wirecheck(i,:,4)';
    yy = yy(yy~=0)+head;
    width = wirecheck(i,:,7)';
    width = width(width~=0);
    single_wire=[xx yy width];
    eval(['wire_' num2str(i) '=single_wire;']);
    
    if i==1
        eval(['save ' matname ' wire_' num2str(i)]);
    else
        eval(['save -append ' matname ' wire_'  num2str(i)]);
    end
    fprintf(fileID, 'Wire measurement: %d.\n', i);
    for k = 1:size(single_wire, 1)  
        fprintf(fileID, '%f\t%f\t%f\n', single_wire(k, 1), ...
            single_wire(k, 2), single_wire(k, 3));
        DrawCircle(single_wire(k, 1), single_wire(k, 2),  single_wire(k, 3)/2, 32, 'r-');
    end
end
fclose(fileID);
fprintf('size: %d.\n', wirenum);
end


function [xbinary, head, left]= resize_convert_pic(x)
    imsize = size(x);%get image size
    if(length(imsize)==3) %indicate whether the image is gray or colorful
        xgray = rgb2gray(x);
    else
        xgray = x;
    end
    [row col] = size(xgray);%get row/col number
    binarythreshold = graythresh(xgray(1:512,1536:end)); %get the threshold for change the image to binary image
    xbinarytmp = im2bw(xgray,binarythreshold); %change the image to binary 
    xbinarytmp=medfilt2(xbinarytmp,[7 7]);%do medfilt for image smoothing, you can refer to matlab's help document for this function.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %imwrite(xbinarytmp,['binary_' imagename],'jpg')%write the image to a jpg file with prefix "binary_"
    xbinary_whole = zeros(row,col);
    xbinary_whole(xbinarytmp==0) = 1;%filp the image

    head = round(row/100);
    tail = round(row/10);
    left = round(col/100);
    right = round(col/100);
    xbinary = xbinary_whole(head:end-tail,left:end-right);%remove the margin
end

function [inwire, outwire, goon]= find_in_out(line, minlength, maxlength)
    goon=0;
    linediff = line(2:end)-line(1:end-1);%get the 1st order filter results(the black/white pix changes)
    if isempty(linediff)
        return;
    end
    inwire = find(linediff == 1);%find the positions for the scan line into the wire(A in Fig 1)
    outwire = find(linediff == -1);%find the positions for the scan line out of the wire(B in Fig 1)
    if isempty(inwire)||isempty(outwire)
        return;
    end
    if inwire(1) > outwire(1)
        outwire = outwire(2:end);%remove extra points
    end

    if inwire(end) > outwire(end)
        inwire = inwire(1:end-1);%%remove extra points
    end
    if isempty(inwire)||isempty(outwire)
        return;
    end
%     for i = 2:length(inwire)
%         inwirediff = inwire(i) - outwire(i-1);
%         if inwirediff <= close_threshold
%             inwire(i) = -1;
%             outwire(i) = -1;
%             inwire(i-1) = -1;
%             outwire(i-1) = -1;
%         end
%     end
%     inwire = inwire(inwire >= 0);
%     outwire = outwire(outwire >= 0);
%     if isempty(inwire)||isempty(outwire)
%         continue;
%     end
    for i = 1:length(inwire)
        inwirediff = outwire(i) - inwire(i);
        if inwirediff <= minlength || inwirediff >= maxlength %remove too narrow measurements
            inwire(i) = -1;
            outwire(i) = -1;
        end
    end
    inwire = inwire(inwire >= 0);
    outwire = outwire(outwire >= 0);
    if isempty(inwire)||isempty(outwire)
        return;
    end
    goon=1;
end


function [validcheck, hori, k, kwidth, wirelength, wiretheta] = get_diameter(xbinary, inwire_ind, outwire_ind,y_coordinate,min_pix_thres, max_pix_thres)
    validcheck = [1 1];
    hori = 0;%indicate whether the wire is horizontal, for now, we don't deal with horizontal ones, but if you want to improve the software in your version, you can do that by rotate the image by 90 degree.
    k = [0 0];%slope of the wire
    kwidth=[0 0];%perpendicular to k
    wirelength = [-1 -1];%see later comments
    wiretheta = 0;
    vlong = 10;%used to remove some bad measurement points
    vshort = 20;%used to remove some bad measurement points
    xlen = outwire_ind-inwire_ind-1;%the length of AB in Fig 1
    vup_o = xbinary(y_coordinate-1:-1:1,outwire_ind);%it is the upper part of a vertical scan line from the outwire point
    vdown_o = xbinary(y_coordinate+1:end,outwire_ind);%it is the lower part of a vertical scan line from the outwire point
    vup_i = xbinary(y_coordinate-1:-1:1,inwire_ind+1);%it is the upper part of a vertical scan line from the inwire point
    vdown_i = xbinary(y_coordinate+1:end,inwire_ind+1);%it is the lower part of a vertical scan line from the inwire point
    %%%% section 1 begins %%%% 
    %%section 1 codes is to get piont C in Fig 1 and compute the slope
    [vup_o_C vup_o_I] = min(vup_o); %get line segment similar to AC 
    [vdown_o_C vdown_o_I] = min(vdown_o);%get line segment similar to AC 
    [vup_i_C vup_i_I] = min(vup_i);%get line segment similar to AC 
    [vdown_i_C vdown_i_I] = min(vdown_i);%get line segment similar to AC 
    if vup_o_I>=vlong && vdown_o_I<=vshort && vup_o_C ~= 1 && vdown_i_I>=vlong && vup_i_I<=vshort && vdown_i_C ~= 1 
        %ensure AC is not too short, and if in the direction from A
        %to C, there is black pixels (pixel in wire) beyond C, this
        %"if" sentence grantee the extra pixel in wire can not be
        %too long
        ylen1 = vup_o_I-1; %line segment similar with AC, from point B
        ylen2 = vdown_i_I-1;%AC
        k = [-ylen1/xlen -ylen2/xlen];
        for side=1:2 %because we can also draw a line similar to AC from B, so there are two sides to process.
           if k(side)~=0
              kwidth(side) = -1/k(side);%perpendicular to k
           else
              validcheck(side) = 0;
           end
        end
        wiretheta = atan(k);%compute the angle
        wirelength = xlen*abs(sin(wiretheta));%this is the wire diameter we want
        for side=1:2
           if wirelength(side) > max_pix_thres || wirelength(side) < min_pix_thres %check wheter it is too narrow/wide
              validcheck(side) = 0;
           end
        end
        hori = 1;
    end 
    if vdown_o_I>=vlong && vup_o_I<=vshort && vdown_o_C ~= 1 && vup_i_I>=vlong && vdown_i_I<=vshort && vup_i_C ~= 1
        %this "if" sentence is to deal with wire with a negative slope; the previous one is for the positive slope 
        ylen1 = vdown_o_I-1;
        ylen2 = vup_i_I-1;
        k = [ylen1/xlen ylen2/xlen];
        for side=1:2
           if k(side)~=0
              kwidth(side) = -1/k(side);
           else
              validcheck(side) = 0;
           end
        end
        wiretheta = atan(k);
        wirelength = xlen*abs(sin(wiretheta));
        for side=1:2
           if wirelength(side) > max_pix_thres || wirelength(side) < min_pix_thres
              validcheck(side) = 0;
           end
        end
        hori = 1;
    end
end

function [pointinrange, totalcount, inpic] = pix_in_range(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k,kwidth)
    totalcount = 0;
    inpic =0;
    pointinrange=[];
    for xx = floor(near(1)) - ceil(detectradius) : 1 : ceil(far(1)) + ceil(detectradius)
        for yy =  floor(near(2))- ceil(detectradius) : 1 : ceil(far(2)) + ceil(detectradius)
            if  yy - k*xx <= upbound &&  yy - k*xx >= downbound && yy - kwidth*xx <= farbound && yy - kwidth*xx >= nearbound
                totalcount = totalcount + 1;
                if  xx >= 1 && xx <= col1 && yy >= 1 && yy <= row1
                    inpic = inpic + 1;
                    pointinrange(inpic,:) = [xx yy]; 
                end
            end
        end
    end
end
function [pointinrange, totalcount, inpic] = pix_in_range_neg(col1,row1,near, far, upbound,downbound,farbound, nearbound,detectradius,k,kwidth)
    totalcount = 0;
    inpic =0;
    pointinrange=[];
    for xx = floor(far(1)) - ceil(detectradius) : 1 : ceil(near(1)) + ceil(detectradius)
        for yy =  floor(near(2))- ceil(detectradius) : 1 : ceil(far(2)) + ceil(detectradius)
            if  yy - k*xx <= upbound &&  yy - k*xx >= downbound && yy - kwidth*xx <= farbound && yy - kwidth*xx >= nearbound
                totalcount = totalcount + 1;
                if  xx >= 1 && xx <= col1 && yy >= 1 && yy <= row1
                    inpic = inpic + 1;
                    pointinrange(inpic,:) = [xx yy]; 
                end
            end
        end
    end
end
function valid = pix_in_wire_ratio(xbinary, pointinrange,inpic,totalcount,inpic2totalcount,numinwire2inpic_check)
    valid=1;
    if inpic/totalcount > inpic2totalcount 
        numinwire = 0;                  
        for kk = 1:inpic
            numinwire = numinwire + xbinary(pointinrange(kk,2),pointinrange(kk,1));
        end               

        if  numinwire/inpic < numinwire2inpic_check %%&& numinwire/totalcount > numinwire2totalcount
            valid = 0;
        end
    else
        valid = 0;
    end
end

function [wirecheck wirenum] = add_wirecheck(wirecheck, wirenum,wirewidth,wirek,inwire,outwire,y_coordinate,i,max_dist)
    if wirewidth ~= -1 
        foundnew = 1;
        if wirenum ~=0
            min_dist=10000;
            min_ind = 0;
            for wire_i=1:wirenum
                [curr_num curr_ptr]=max(wirecheck(wire_i,:,4));                  
                dist1 = abs(wirecheck(wire_i,curr_ptr,5)*(inwire(i)+outwire(i)+1)/2+wirecheck(wire_i,curr_ptr,6)-y_coordinate)/sqrt(1+wirecheck(wire_i,curr_ptr,5)^2);
                dist2 = abs(wirek*wirecheck(wire_i,curr_ptr,3)+(y_coordinate - wirek*(inwire(i)+outwire(i)+1)/2)-wirecheck(wire_i,curr_ptr,4))/sqrt(1+wirek^2);
                %dist1 is the distance between the middle of AB and the
                %line generated by existing wire's last measurement

                %dist2 is the distance between the existing wire's last
                %measurement middle point and the line generated by the
                %current measurement
                dist = dist1+dist2;
                if dist<min_dist
                   min_dist=dist;
                   min_ind = wire_i;
                end
            end
            wire_i=min_ind;
            [curr_num curr_ptr]=max(wirecheck(wire_i,:,4));                  
            dist1 = abs(wirecheck(wire_i,curr_ptr,5)*(inwire(i)+outwire(i)+1)/2+wirecheck(wire_i,curr_ptr,6)-y_coordinate)/sqrt(1+wirecheck(wire_i,curr_ptr,5)^2);
            dist2 = abs(wirek*wirecheck(wire_i,curr_ptr,3)+(y_coordinate - wirek*(inwire(i)+outwire(i)+1)/2)-wirecheck(wire_i,curr_ptr,4))/sqrt(1+wirek^2);
            if dist1<max_dist && dist2<max_dist
                wirecheck(wire_i,curr_ptr+1,1)= inwire(i)+1;%store the inwire point
                wirecheck(wire_i,curr_ptr+1,2)= outwire(i);%store the outwire point
                wirecheck(wire_i,curr_ptr+1,3)= (inwire(i)+1+outwire(i))/2;%store the mid point
                wirecheck(wire_i,curr_ptr+1,4)= y_coordinate;%store the y coordinate
                wirecheck(wire_i,curr_ptr+1,5)= wirek; %store the slope
                wirecheck(wire_i,curr_ptr+1,6)= y_coordinate-(inwire(i)+outwire(i)+1)/2*wirek;%this is for convenience of later computation
                wirecheck(wire_i,curr_ptr+1,7)= wirewidth;  %store the diameter               
                foundnew = 0;
            end

        else             
            foundnew = 1;
        end
        if foundnew == 1
            wirenum=wirenum+1;
            wirecheck(wirenum,1,1)=inwire(i)+1;
            wirecheck(wirenum,1,2)=outwire(i);
            wirecheck(wirenum,1,3)=(inwire(i)+1+outwire(i))/2;
            wirecheck(wirenum,1,4)=y_coordinate; 
            wirecheck(wirenum,1,5)=wirek;
            wirecheck(wirenum,1,6)=y_coordinate-(inwire(i)+outwire(i)+1)/2*wirek;
            wirecheck(wirenum,1,7)=wirewidth;                        
        end
    end       
end


