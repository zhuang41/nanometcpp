clear;
clc;
close all;

delete ResultsCPP.txt;
delete ResultsMATLAB.txt;

%% run MATLAB code
tic;
imageDir = 'C:\Users\Yao\Documents\Data\nano\wire\';
filenamePre = '1';
filenameExt = '.jpg';
imageFilename = [imageDir filenamePre filenameExt];
step = 40;
scaleFactor = 0.5;
batpro(imageFilename, step, scaleFactor);
toc;

%% run CPP code
tic;
exeFilename = 'C:\Users\Yao\Documents\sandboxes\nano\cpp\x64\Debug\nano-wire.exe';
argv1 = imageFilename;
command = exeFilename;
system(command);
toc;

%% compare results
figure(2);
image = imread('ResultsCPP.jpg');
imshow(image);