clear;
clc;
close all;

%% run MATLAB code
tic;
imageDir = 'C:\Users\Yao\Documents\Data\nano\particle\';
filenamePre = '1';
filenameExt = '.jpg';
imageFilename = [imageDir filenamePre filenameExt];
step = 10;
scaleFactor = 0.5;
particle_pro(imageFilename, step, scaleFactor);
toc;

%% run CPP code
tic;
exeFilename = 'C:\Users\Yao\Documents\sandboxes\nano\cpp\x64\Debug\nano-particle.exe';
argv1 = imageFilename;
argv2 = '-1.0';
command = [exeFilename ' ' argv1 ' ' argv2];
system(command);
toc;

%% show results
resultCPP = imread('ResultCPP.jpg');
figure(2);
imshow(resultCPP);
title('OpenCV Result');

