function particlePro(imagename, step, scale)
    warning off all
    x = imread(imagename);
    [xbinary row col] = resize_convert_pic(x);
    
    %%% parameter to tune
    max_dist = 20;
    minlength = 50;
    maxlength = 100;
    %%%
    upmin = 2;
    downmin=2;
    var_tol=5;
    particle_num=0;
    particle=zeros(1000,1000,7);
    for y_coordinate = step:step:row-step%10:5:row1
        line = xbinary(y_coordinate,:);%get a line of pix
        [inwire, outwire, goon]= find_in_out(line, minlength, maxlength);
        if (goon == 0)
           continue; 
        end
        for i = 1:length(inwire)
%             DrawCircle(inwire(i), y_coordinate, 4, 32, 'g-');
%             DrawCircle(outwire(i), y_coordinate, 4, 32, 'r-');
            final_cir = find_circle(inwire(i), outwire(i),y_coordinate,xbinary,upmin,downmin,row,step,var_tol);
            if isempty(final_cir)~=1
                [particle particle_num] = add_check_circle(final_cir,particle_num,particle,max_dist);
            end
        end   
    end
    
    
    if ~exist('detail','dir')
        mkdir('detail');
    end
    matname = 'detail/Results.mat';
    if exist(matname,'file')
       delete(matname) 
    end
    
    txtname = 'ResultsMATLAB.txt';
    if exist(txtname,'file')
       delete(txtname);
    end
    fileID = fopen(txtname, 'w');
    
    firstcol=particle(:,1,1);
    firstcol = firstcol(firstcol~=0);
    rownum = length(firstcol);
    num=1;
    
    figure(1);
    himage = imshow(xbinary);
    title('MATLAB Result');
    hold on;
    
    % for i = 1:rownum
    for i = 1:rownum
        xx = particle(i,:,1);
        xx = xx(xx~=0);
        confidence=length(xx)/4;
        if(confidence>1)
           confidence=1; 
        end
        x = mean(xx);
        yy = particle(i,:,2);
        yy = yy(yy~=0);
        y = mean(yy);
        rr = particle(i,:,3);
        rr = rr(rr~=0);
        r = mean(rr);
        single_par=[x y r confidence];
%         eval(['particle_' num2str(num) '=single_par;']);
%         if ~exist(matname,'file')
%             eval(['save ' matname ' particle_' num2str(num)]);
%         else
%             eval(['save -append ' matname ' particle_'  num2str(num)]);
%         end
%         if(confidence==1/4)
%           DrawCircle(x, y, r, 32, 'b-');
%         end
%         if(confidence==1/2)
%           DrawCircle(x, y, r, 32, 'g-');
%         end
%         if(confidence==3/4)
%           DrawCircle(x, y, r, 32, 'y-');
%         end
%         if(confidence>=1)
%           DrawCircle(x, y, r, 32, 'r-');
%         end
        
        %if(confidence>=2/4)
            eval(['particle_' num2str(num) '=single_par;']);
            if ~exist(matname,'file')
            eval(['save ' matname ' particle_' num2str(num)]);
            else
            eval(['save -append ' matname ' particle_'  num2str(num)]);
            end
            DrawCircle(x, y, r, 32, 'r-');
            num=num+1;
        %end
        
        fprintf(fileID, '%f\t%f\t%f\t%f\n', x, y, r, confidence);
    end
    fclose(fileID);
    hold off
    saveas(himage, 'ResultMATLAB.jpg')
end

function [xbinary row col]= resize_convert_pic(x)
    imsize = size(x);%get image size
    if(length(imsize)==3) %indicate whether the image is gray or colorful
        xgray = rgb2gray(x);
    else
        xgray = x;
    end
    [row col] = size(xgray);%get row/col number
    binarythreshold = graythresh(xgray); %get the threshold for change the image to binary image
    [row col] = size(xgray);
    xbinarytmp = im2bw(xgray,binarythreshold);
    xbinarytmp=medfilt2(xbinarytmp,[7 7]);
    xbinary = zeros(row,col);
    xbinary(xbinarytmp==0) = 1;
    % xbinary=medfilt2(xbinary,[3,3]);
end

function [inwire, outwire, goon]= find_in_out(line, minlength, maxlength)
    goon=0;
    linediff = line(2:end)-line(1:end-1);%get the 1st order filter results(the black/white pix changes)
    if isempty(linediff)
        return;
    end
    inwire = find(linediff == 1);%find the positions for the scan line into the wire(A in Fig 1)
    outwire = find(linediff == -1);%find the positions for the scan line out of the wire(B in Fig 1)
    if isempty(inwire)||isempty(outwire)
        return;
    end
    if inwire(1) > outwire(1)
        outwire = outwire(2:end);%remove extra points
    end

    if inwire(end) > outwire(end)
        inwire = inwire(1:end-1);%%remove extra points
    end
    if isempty(inwire)||isempty(outwire)
        return;
    end
    for i = 1:length(inwire)
        inwirediff = outwire(i) - inwire(i);
        if inwirediff <= minlength || inwirediff >= maxlength %remove too narrow measurements
            inwire(i) = -1;
            outwire(i) = -1;
        end
    end
    inwire = inwire(inwire >= 0);
    outwire = outwire(outwire >= 0);
    if isempty(inwire)||isempty(outwire)
        return;
    end
    goon=1;

end
function final_cir = find_circle(inwire_num, outwire_num,y_coordinate,xbinary,upmin,downmin,row,step,var_tol)
    len = outwire_num-inwire_num;
    header = len/10;   
    line_cir=[];
    for j = ceil(inwire_num+header):floor(outwire_num-header) %don't search for the beginning and ending
        upline = xbinary(y_coordinate-1:-1:1,j);
        downline = xbinary(y_coordinate+1:end,j);
        [up_C up] = min(upline);
        [down_C down] = min(downline);

        if (up>upmin)&&(down>downmin)&&(up+down>len*0.8)&&(up+down<2*len)&&(y_coordinate-up>step)&&(y_coordinate++down<row-step)
            p(1,:)=[inwire_num+1 y_coordinate];
            p(2,:)=[outwire_num y_coordinate];
            p(3,:)=[j y_coordinate-up];
            p(4,:)=[j y_coordinate+down];
            tot=[];
            for pind=1:4
                XY = p;
                XY(pind,:)=[];
                circle = CircleFitByTaubin(XY);
                val = isnan(circle);
                valind = find(val);
                if isempty(valind)
                    tot=[tot;circle];                     
                end            
            end
            if ~isempty(tot)
                cir_var=std(tot,1);
                var_ind = find(cir_var>=var_tol);
                if isempty(var_ind)
                    line_cir=[line_cir;mean(tot,1)];
                end
            end
        end
    end 
    final_cir = mean(line_cir,1);
end
function [particle_out particle_num] = add_check_circle(final_cir,particle_num,particle,max_dist)
    if ~isempty(final_cir)
        foundnew = 1;
        if particle_num~=0 
            for particle_i=1:particle_num
                curr_ptr=length(find(particle(particle_i,:,1)>0)); 
                x=sum(particle(particle_i,:,1))/curr_ptr;
                y=sum(particle(particle_i,:,2))/curr_ptr;
                r=sum(particle(particle_i,:,3))/curr_ptr;
                dist = sqrt((x-final_cir(1))^2+(y-final_cir(2))^2);
%                     dist=pdist([x y;final_cir(1) final_cir(2)]);
%                 dev = abs(r-final_cir(3));
                if dist < max_dist
                    particle(particle_i,curr_ptr+1,1)= final_cir(1);
                    particle(particle_i,curr_ptr+1,2)= final_cir(2);
                    particle(particle_i,curr_ptr+1,3)= final_cir(3); 
                    foundnew=0;
                end
            end
        end
        if foundnew==1
            particle_num = particle_num+1;
            particle(particle_num,1,1)= final_cir(1);
            particle(particle_num,1,2)= final_cir(2);
            particle(particle_num,1,3)= final_cir(3);
        end
    end
    particle_out=particle;
end
