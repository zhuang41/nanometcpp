clear all
close all
imagename='C:\Users\Yao\Documents\data\nano\rupture\1.tif';
x=imread(imagename);
%imshow(x);
figure
imshow(x);
hold on
imsize = size(x);
if(length(imsize)==3) 
    xgray = rgb2gray(x);
else
    xgray = x;
end
[row col] = size(xgray);
xgray=xgray(1:row-60,:);% get rid of the tag in the bottom
[row col] = size(xgray);
binarythreshold = 0.38;%binarythreshold = graythresh(xgray);Manually choose the threshold by seeing the image
xbinary_orig = im2bw(xgray,binarythreshold);
xbinary_orig=medfilt2(xbinary_orig,[7 7]);
xbinary = zeros(row,col);
xbinary(xbinary_orig==0) = 1;
%imshow(xbinary);
[L, num] = bwlabel(xbinary, 8);
S = regionprops(L, 'Area','Centroid');
Area=[];
for i=1:length(S)
    Area(i)=S(i).Area;
end
Area = sort(Area,'descend');
P=Area(40);%choose the threshold to exclude the regions with too small area
regions = find([S.Area] >= P);
bw2 = ismember(L, regions);
[xc,yc]=find(bw2>0);
%S(regions(1)).Centroid;
plot(yc,xc,'.r')
hold on
for i=1:length(regions)
   str=num2str(S(regions(i)).Area);
   text(S(regions(i)).Centroid(1),S(regions(i)).Centroid(2),str)
end